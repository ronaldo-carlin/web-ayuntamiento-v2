//consulta para rellenar select de periodos segun la seccion
function period(btn) {
  let id = $(btn).data("section_id");
  let name = $(btn).data("name");
  var period_id = $(`#period${id}`);

  //console.log(id);

  $data = {
    opt: "period",
    id: id,
  };

  //consulta para tabla de secciones
  envioAjax($data, "./index.php?action=ajaxrequest")
    .done((response) => {
      var data = response["sections"];

      //console.log(data);
      //console.log(response.section[0].id);

      if (data.length > 0) {
        //alert(`si hay datos con el id=  ${id}`);

        // Limpiamos el select
        period_id.find("option").remove();

        let i = 0;
        $.each(data, function (i, id, name) {
          if (i == 0) {
            //console.log(data[i].id);
            period_id.append(
              '<option value="' + data[i].id + '">' + data[i].name + "</option>"
            );
            ShowSelectedid(data[i].id);
            i++;
          } else {
            period_id.append(
              '<option value="' + data[i].id + '">' + data[i].name + "</option>"
            );
            i++;
          }
        });

        period_id.prop("disabled", false);
      } else {
        $("#period" + id).remove();
        $(btn).remove();
        alert(`Aun no hay periodos de esta sección ${name}`);
        let sectionanterior = name - 1;
        $("#section-" + sectionanterior + "-tab").get(0).click();
      }
    })
    .fail((jqXHR) => {
      console.log(jqXHR);
    });
}

//funcion para agregar los elementos
function ShowSelectedid(id) {
  let value = id;
  let li = document.querySelector("#descarga");
  let li2 = document.querySelector("#descarga2");
  let li3 = document.querySelector("#descarga3");
  let li4 = document.querySelector("#descarga4");

  //alert(value);

  $data = {
    opt: "element",
    id: value,
  };

  envioAjax($data, "./index.php?action=ajaxrequest")
    .done((response) => {
      var data = response["elements"];

      //console.log(data[0].link);

      if (data.length > 0) {
        //$("#descarga").attr("href", data[0].link);
        li.innerHTML = "";

        if (li2 != null) {
          li2.innerHTML = "";
        }

        if (li3 != null) {
          li3.innerHTML = "";
        }

        if (li4 != null) {
          li4.innerHTML = "";
        }

        for (let item of data) {
          //console.log(item.id);

          li.innerHTML += `
                <li>
                  <a id="descarga1" href='${item.link}' target="_blank">
                    <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
                    <h5 class="font-size-14 m-0">Descargar</h5>
                    <span>Información</span>
                  </a>
                </li>
                `;

          if (li2 != null) {
            li2.innerHTML += `
                <li>
                  <a id="descarga2" href='${item.link}' target="_blank">
                    <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
                    <h5 class="font-size-14 m-0">Descargar</h5>
                    <span>Información</span>
                  </a>
                </li>
                `;
          }
          if (li3 != null) {
            li3.innerHTML += `
                <li>
                  <a id="descarga3" href='${item.link}' target="_blank">
                    <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
                    <h5 class="font-size-14 m-0">Descargar</h5>
                    <span>Información</span>
                  </a>
                </li>
                `;
          }
          if (li4 != null) {
            li4.innerHTML += `
                <li>
                  <a id="descarga4" href='${item.link}' target="_blank">
                    <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
                    <h5 class="font-size-14 m-0">Descargar</h5>
                    <span>Información</span>
                  </a>
                </li>
                `;
          }
        }
      } else {
        li.innerHTML = "";

        if (li2 != null) {
          li2.innerHTML = "";
        }

        if (li3 != null) {
          li3.innerHTML = "";
        }

        if (li4 != null) {
          li4.innerHTML = "";
        }

        console.log("error");
        li.innerHTML += `
        <li>
            <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
            <h5 class="font-size-14 m-0">No hay Descargas Diponibles</h5>
            <span>Sin Información</span>
        </li>
          `;
        if (li2 != null) {
          li2.innerHTML += `
        <li>
            <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
            <h5 class="font-size-14 m-0">No hay Descargas Diponibles</h5>
            <span>Sin Información</span>
        </li>
          `;
        }
        if (li3 != null) {
          li3.innerHTML += `
        <li>
            <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
            <h5 class="font-size-14 m-0">No hay Descargas Diponibles</h5>
            <span>Sin Información</span>
        </li>
          `;
        }
        if (li4 != null) {
          li4.innerHTML += `
        <li>
            <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
            <h5 class="font-size-14 m-0">No hay Descargas Diponibles</h5>
            <span>Sin Información</span>
        </li>
          `;
        }
      }
    })
    .fail((jqXHR) => {
      console.log(jqXHR);
    });
}

//funcion para cada vez que cambian de option
function ShowSelected(selectObject) {
  let value = selectObject.value;
  let li = document.querySelector("#descarga");
  let li2 = document.querySelector("#descarga2");
  let li3 = document.querySelector("#descarga3");
  let li4 = document.querySelector("#descarga4");

  //alert(value);

  $data = {
    opt: "element",
    id: value,
  };

  envioAjax($data, "./index.php?action=ajaxrequest")
    .done((response) => {
      var data = response["elements"];

      //console.log(data[0].link);

      if (data.length > 0) {
        //$("#descarga").attr("href", data[0].link);
        li.innerHTML = "";
        if (li2 != null) {
          li2.innerHTML = "";
        }
        if (li3 != null) {
          li3.innerHTML = "";
        }
        if (li4 != null) {
          li4.innerHTML = "";
        }

        for (let item of data) {
          //console.log(item.id);
          li.innerHTML += `
                <li>
                  <a id="descarga1" href='${item.link}' target="_blank">
                    <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
                    <h5 class="font-size-14 m-0">Descargar</h5>
                    <span>Información</span>
                  </a>
                </li>
                `;

          if (li2 != null) {
            li2.innerHTML += `
                <li>
                  <a id="descarga2" href='${item.link}' target="_blank">
                    <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
                    <h5 class="font-size-14 m-0">Descargar</h5>
                    <span>Información</span>
                  </a>
                </li>
                `;
          }
          if (li3 != null) {
            li3.innerHTML += `
                <li>
                  <a id="descarga3" href='${item.link}' target="_blank">
                    <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
                    <h5 class="font-size-14 m-0">Descargar</h5>
                    <span>Información</span>
                  </a>
                </li>
                `;
          }
          if (li4 != null) {
            li4.innerHTML += `
                <li>
                  <a id="descarga4" href='${item.link}' target="_blank">
                    <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
                    <h5 class="font-size-14 m-0">Descargar</h5>
                    <span>Información</span>
                  </a>
                </li>
                `;
          }
        }
      } else {
        li.innerHTML = "";
        if (li2 != null) {
          li2.innerHTML = "";
        }
        if (li3 != null) {
          li3.innerHTML = "";
        }
        if (li4 != null) {
          li4.innerHTML = "";
        }

        console.log("error");
        li.innerHTML += `
        <li>
            <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
            <h5 class="font-size-14 m-0">No hay Descargas Diponibles</h5>
            <span>Sin Información</span>
        </li>
          `;
        if (li2 != null) {
          li2.innerHTML += `
        <li>
            <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
            <h5 class="font-size-14 m-0">No hay Descargas Diponibles</h5>
            <span>Sin Información</span>
        </li>
          `;
        }
        if (li3 != null) {
          li3.innerHTML += `
        <li>
            <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
            <h5 class="font-size-14 m-0">No hay Descargas Diponibles</h5>
            <span>Sin Información</span>
        </li>
          `;
        }
        if (li4 != null) {
          li4.innerHTML += `
        <li>
            <i class="ri-download-2-line mb-3" style="display: block; font-size: 10rem;"></i>
            <h5 class="font-size-14 m-0">No hay Descargas Diponibles</h5>
            <span>Sin Información</span>
        </li>
          `;
        }
      }
    })
    .fail((jqXHR) => {
      console.log(jqXHR);
    });
}

function envioAjax(datas = [], url) {
  var jqXHR = $.ajax({
    url: url,
    dataType: "JSON",
    type: "POST",
    data: datas,
  });

  return jqXHR;
}
