<?php
$id = ($_GET['id_noticia']);
$url = $GLOBALS["api"].'/getNewsById?id=' . $id;
$json = file_get_contents($url);
$array = json_decode($json, true);

$url2 = $GLOBALS["api"].'/getDepartments';
$json2 = file_get_contents($url2);
$departament = json_decode($json2, true);

$url3 = $GLOBALS["api"].'/getAllNewsLimit';
$json3 = file_get_contents($url3);
$newslimit = json_decode($json3, true);

$multi = ltrim($array['news'][0]['multimedia'], ',');
$video = ltrim($array['news'][0]['video'], ',');
$link = ltrim($array['news'][0]['link_youtube'], ',');
$multi2 = explode(",", $multi);
$video2 = explode(",", $video);
$link2 = explode(",", $link);
$video_url = $array['news'][0]['link_youtube'];
$departments = $array['news'][0]['departments'];
$cover_page = $array['news'][0]['cover_page'];
$created_by = $array['news'][0]['name_user'];
$titulo = $array['news'][0]['title'];
$descripcion = $array['news'][0]['description'];

/* echo '<pre>';
print_r($array);
echo '</pre>'; */

/* FECHA */
$mes = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$mesNoticia = date('n', strtotime($array['news'][0]['created_at']));
$diaNoticia = date('d', strtotime($array['news'][0]['created_at']));
$anioNoticia = date('Y', strtotime($array['news'][0]['created_at']));
setlocale(LC_TIME, "spanish");
?>

<!-- FUNCIÓN ACTIVE MENÚ -->
<script type="text/javascript">
    let elemento = document.getElementById("li-noticias");
    elemento.className += " active";
    let elemento2 = document.getElementById("li-prensa");
    elemento2.className += " active";
</script>

<!-- Content Wrapper Start -->
<div class="content-wrapper">

    <!-- Breadcrumb Start -->
    <div class="breadcrumb-wrap bg-spring">
        <img src="assets/img/breadcrumb/br-shape-1.png" alt="Image" class="br-shape-one xs-none">
        <img src="assets/img/breadcrumb/br-shape-2.png" alt="Image" class="br-shape-two xs-none">
        <img src="assets/img/breadcrumb/br-shape-3.png" alt="Image" class="br-shape-three moveHorizontal sm-none">
        <img src="assets/img/breadcrumb/br-shape-4.png" alt="Image" class="br-shape-four moveVertical sm-none">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7 col-md-8 col-sm-8">
                    <div class="breadcrumb-title">
                        <h2>Noticia Detallada</h2>
                        <ul class="breadcrumb-menu list-style">
                            <li><a href="./index.php?view=home">Inicio </a></li>
                            <li><a href="blog-right-sidebar.html">Noticias </a></li>
                            <li>Noticia detallada</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-5 col-md-4 col-sm-4 xs-none">
                    <div class="breadcrumb-img">
                        <img src="assets/img/breadcrumb/br-shape-5.png" alt="Image" class="br-shape-five animationFramesTwo">
                        <img src="assets/img/breadcrumb/br-shape-6.png" alt="Image" class="br-shape-six bounce">
                        <img src="assets/img/breadcrumb/breadcrumb-4.png" alt="Image">
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Blog Details Section Start -->
    <div class="blog-details-wrap ptb-100">
        <div class="container">
            <div class="row gx-5">

                <div class="col-xl-8 col-lg-12">
                    <article>

                        <ul class="post-metainfo  list-style">
                            <li><i class="flaticon-user"></i><a href="posts-by-author.html"><?php echo $created_by; ?></a></li>
                            <li><i class="flaticon-calendar"></i><a href="posts-by-date.html"><?php echo $diaNoticia . ' - ' . $mesNoticia . ' - ' . $anioNoticia; ?></a></li>
                            <!-- <li><i class="flaticon-bubble-chat"></i>No Comment</li> -->
                        </ul>

                        <h2><?php echo $titulo; ?></h2>

                        <div class="post-para">
                            <p><?php echo $descripcion; ?></p>
                        </div>

                        <?php if ($array['news'][0]['video'] != "") : ?>
                            <div class="col-lg-12">
                                <div class="div_contenedor">
                                    <video class="img-fluid" controls>
                                        <source src= "<?php echo $GLOBALS["multimedia"]."/files/videoNewsWeb/".$array['news'][0]['video']; ?>" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        <?php endif;  ?>

                        <?php if ($array['news'][0]['link_youtube'] != "") : ?>
                            <div class="col-lg-12">
                                <div class="div_contenedor">
                                    <iframe loading="lazy" type="text/html" src="<?php echo $video_url ?>" frameborder="0"></iframe>
                                    <!-- <?php echo "<iframe width=\"420\" height=\"315\" src=\"http://www.youtube.com/embed/" . $video_url . "\" frameborder=\"0\" allowfullscreen></iframe>" ?> -->
                                </div>
                            </div>
                        <?php endif ?>

                        <div class="post-img mt-3">
                            <?php if ($array['news'][0]['multimedia'] != "") : ?>
                                <div class="col-lg-12">
                                    <div class="carousel slide" data-bs-ride="carousel">
                                        <div class="carousel-inner">
                                            <?php $count = 0;
                                            foreach ($multi2 as $m) :
                                                if ($count == 0) :
                                                    $count++;
                                            ?>
                                                    <div class="carousel-item active">
                                                        <img src="<?php echo $GLOBALS["multimedia"]."/files/imgNews/".$m; ?>" class="img-fluid h-100" alt="Image">
                                                    </div>
                                                <?php else : ?>
                                                    <div class="carousel-item">
                                                        <img src="<?php echo $GLOBALS["multimedia"]."/files/imgNews/".$m; ?>" class="img-fluid h-100" alt="Image">
                                                    </div>
                                            <?php endif;
                                            endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif  ?>
                        </div>

                    </article>

                    <div class="post-meta-option">
                        <div class="row gx-0 align-items-center">
                            <div class="col-md-7 col-12">
                                <div class="post-tag">
                                    <span> <i class="flaticon-supermarket"></i>Departamento:</span>
                                    <ul class="tag-list list-style">
                                        <li><a href="posts-by-tag.html"><?php echo $departments ?></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-5 col-12 text-md-end text-start">
                                <div class="post-share w-100">
                                    <span>Compartir:</span>
                                    <ul class="social-profile style2 list-style">
                                        <li>
                                            <a target="_blank" href="https://facebook.com">
                                                <i class="ri-facebook-fill"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a target="_blank" href="https://twitter.com">
                                                <i class="ri-twitter-fill"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a target="_blank" href="https://linkedin.com">
                                                <i class="ri-linkedin-fill"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a target="_blank" href="https://instagram.com">
                                                <i class="ri-pinterest-fill"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="post-author">
                        <div class="post-author-img">
                            <img src="assets/img/blog/author-1.jpg" alt="Image">
                        </div>
                        <div class="post-author-info">
                            <h4>Fransis Josef</h4>
                            <p>Claritas est etiam amet sinicus, qui sequitur lorem ipsum semet coui lectorum. Lorem ipsum dolor voluptatem corporis blanditiis sadipscing elitr sed diam nonumy eirmod amet sit lorem dolor.</p>
                            <ul class="social-profile style2 list-style">
                                <li>
                                    <a target="_blank" href="https://facebook.com">
                                        <i class="ri-facebook-fill"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://twitter.com">
                                        <i class="ri-twitter-fill"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://linkedin.com">
                                        <i class="ri-linkedin-fill"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="comment-box-wrap">
                        <div class="comment-box-title mb-30">
                            <h4><span>3</span> Comments</h4>
                        </div>
                        <div class="comment-item-wrap">
                            <div class="comment-item">
                                <div class="comment-author-img">
                                    <img src="assets/img/blog/author-2.jpg" alt="mage">
                                </div>
                                <div class="comment-author-wrap">
                                    <div class="comment-author-info">
                                        <div class="row align-items-start">
                                            <div class="col-md-9 col-sm-12 col-12 order-md-1 order-sm-1 order-1">
                                                <div class="comment-author-name">
                                                    <h5>Olivic Dsuza <span class="comment-date">Oct 22, 2021</span></h5>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-12 text-md-end order-md-2 order-sm-3 order-3">
                                                <a href="#cmt-form" class="reply-btn">Reply</a>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-12 order-md-3 order-sm-2 order-2">
                                                <div class="comment-text">
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                                                        sed diam nonumy eirmod tempor invidunt ut labore et dolore
                                                        magna aliquyam erat.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-item reply">
                                <div class="comment-author-img">
                                    <img src="assets/img/blog/author-3.jpg" alt="mage">
                                </div>
                                <div class="comment-author-wrap">
                                    <div class="comment-author-info">
                                        <div class="row align-items-start">
                                            <div class="col-md-9 col-sm-12 col-12 order-md-1 order-sm-1 order-1">
                                                <div class="comment-author-name">
                                                    <h5>Everly Leah <span class="comment-date">Oct 30, 2021</span></h5>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-12 text-md-end order-md-2 order-sm-3 order-3">
                                                <a href="#cmt-form" class="reply-btn">Reply</a>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-12 order-md-3 order-sm-2 order-2">
                                                <div class="comment-text">
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                                                        sed diam nonumy eirmod tempor invidunt ut labore et dolore
                                                        magna aliquyam erat.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-item">
                                <div class="comment-author-img">
                                    <img src="assets/img/blog/author-4.jpg" alt="mage">
                                </div>
                                <div class="comment-author-wrap">
                                    <div class="comment-author-info">
                                        <div class="row align-items-start">
                                            <div class="col-md-9 col-sm-12 col-12 order-md-1 order-sm-1 order-1">
                                                <div class="comment-author-name">
                                                    <h5>Ella Jackson <span class="comment-date">Sep 15, 2021</span></h5>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-12 text-md-end order-md-2 order-sm-3 order-3">
                                                <a href="#cmt-form" class="reply-btn">Reply</a>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-12 order-md-3 order-sm-2 order-2">
                                                <div class="comment-text">
                                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                                                        sed diam nonumy eirmod tempor invidunt ut labore et dolore
                                                        magna aliquyam erat.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="cmt-form">
                        <div class="comment-box-title mb-25">
                            <h4>Leave A Comment</h4>
                            <p>Your email address will not be published. Required fields are marked.</p>
                        </div>
                        <form action="#" class="comment-form">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" name="name" id="name" required placeholder="Name*" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" required placeholder="Email Address*" required>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea name="messages" id="messages" cols="30" rows="10" placeholder="Please Enter Your Comment Here"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <div class="checkbox style2">
                                        <input type="checkbox" id="test_1">
                                        <label for="test_1">Save my name,email,website addres in this browser for the next time I commnet.</label>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-20">
                                    <button class="btn style1">Post A Comment</button>
                                </div>
                            </div>
                        </form>
                    </div> -->
                </div>

                <div class="col-xl-4 col-lg-12">
                    <div class="sidebar">

                        <div class="sidebar-widget style4">
                            <div class="search-box">
                                <div class="form-group">
                                    <input type="search" placeholder="Buscar">
                                    <button type="submit">
                                        <i class="flaticon-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="sidebar-widget categories">
                            <h4>Categorias</h4>
                            <div class="category-box">
                                <ul class="list-style">
                                    <?php $count = 0;
                                    foreach ($departament['departments'] as $d) :
                                        $count++ ?>
                                        <li>
                                            <a href="#">
                                                <i class="ri-login-box-line"></i><?php echo $d['name']; ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                    <!-- <li>
                                        <a href="posts-by-category.html">
                                            <i class="ri-login-box-line"></i>
                                            Business Card
                                            <span>(2)</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="posts-by-category.html">
                                            <i class="ri-login-box-line"></i>
                                            Uncategorized
                                            <span>(6)</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="posts-by-category.html">
                                            <i class="ri-login-box-line"></i>
                                            Online Banking
                                            <span>(5)</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="posts-by-category.html">
                                            <i class="ri-login-box-line"></i>
                                            Digital Payment
                                            <span>(9)</span>
                                        </a>
                                    </li> -->
                                </ul>
                            </div>
                        </div>

                        <div class="sidebar-widget popular-post">
                            <h4>Noticias recientes</h4>
                            <div class="popular-post-widget">
                                <?php $count = 0;
                                foreach ($newslimit['news'] as $n) :
                                    $count++ ?>
                                    <div class="pp-post-item">
                                        <a href="index.php?view=noticia_detallada&id_noticia=<?php echo $n['id'] ?>" class="pp-post-img">
                                            <img loading="lazy" src="<?php echo $GLOBALS["multimedia"]."/files/imgNews/".$n['cover_page']; ?>" alt="Image" style="width: 80px;">
                                        </a>
                                        <div class="pp-post-info">
                                            <span><i class="flaticon-calendar"></i>
                                                <?php
                                                $campo = date($n['created_at']);
                                                $date = date_create($campo);
                                                $fecha = date_format($date, 'd-m-Y');
                                                echo $fecha;
                                                ?>
                                            </span>
                                            <h6>
                                                <a href="index.php?view=noticia_detallada&id_noticia=<?php echo $n['id'] ?>">
                                                    <?php echo $n['title']; ?>
                                                </a>
                                            </h6>
                                        </div>
                                    </div>
                                    <!-- <div class="pp-post-item">
                                        <a href="blog-details-right-sidebar.html" class="pp-post-img">
                                            <img src="assets/img/blog/post-thumb-2.jpg" alt="Image">
                                        </a>
                                        <div class="pp-post-info">
                                            <span><i class="flaticon-calendar"></i>25 Sep, 2021</span>
                                            <h6>
                                                <a href="blog-details-no-sidebar.html">
                                                    Available Financial Loans For Business
                                                </a>
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="pp-post-item">
                                        <a href="blog-details-right-sidebar.html" class="pp-post-img">
                                            <img src="assets/img/blog/post-thumb-3.jpg" alt="Image">
                                        </a>
                                        <div class="pp-post-info">
                                            <span><i class="flaticon-calendar"></i>12 Sep, 2021</span>
                                            <h6>
                                                <a href="blog-details-no-sidebar.html">
                                                    5 Benefit Of Credit Card In Payment
                                                </a>
                                            </h6>
                                        </div>
                                    </div> -->
                                <?php endforeach; ?>
                            </div>
                        </div>

                        <!-- <div class="sidebar-widget tags">
                            <h4>Popular Tags </h4>
                            <div class="tag-list">
                                <ul class="list-style">
                                    <li><a href="posts-by-tag.html">Business</a></li>
                                    <li><a href="posts-by-tag.html">Costs</a></li>
                                    <li><a href="posts-by-tag.html">Interest</a></li>
                                    <li><a href="posts-by-tag.html">Loan</a></li>
                                    <li><a href="posts-by-tag.html">Payment</a></li>
                                    <li><a href="posts-by-tag.html">Cards</a></li>
                                </ul>
                            </div>
                        </div> -->

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Blog Details Section End -->

</div>
<!-- Content Wrapper End -->