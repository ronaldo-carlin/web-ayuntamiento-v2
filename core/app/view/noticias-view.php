<?php
$url = $GLOBALS["api"].'/getNewsPublic';
$json = file_get_contents($url);
$array = json_decode($json, true);

$url2 = $GLOBALS["api"].'/getDepartments';
$json2 = file_get_contents($url2);
$departament = json_decode($json2, true);

$url3 = $GLOBALS["api"].'/getAllNewsLimit';
$json3 = file_get_contents($url3);
$newslimit = json_decode($json3, true);

setlocale(LC_TIME, "spanish");

/* echo '<pre>';
print_r($array['fractions']);
var_dump($_GET);
echo '</pre>'; */


?>

<!-- FUNCIÓN ACTIVE MENÚ -->
<script type="text/javascript">
    let elemento = document.getElementById("li-noticias");
    elemento.className += " active";
    let elemento2 = document.getElementById("li-prensa");
    elemento2.className += " active";
</script>

<!-- Content Wrapper Start -->
<div class="content-wrapper">

    <!-- Breadcrumb Start -->
    <div class="breadcrumb-wrap bg-spring">
        <img src="assets/img/breadcrumb/br-shape-1.png" alt="Image" class="br-shape-one xs-none">
        <img src="assets/img/breadcrumb/br-shape-2.png" alt="Image" class="br-shape-two xs-none">
        <img src="assets/img/breadcrumb/br-shape-3.png" alt="Image" class="br-shape-three moveHorizontal sm-none">
        <img src="assets/img/breadcrumb/br-shape-4.png" alt="Image" class="br-shape-four moveVertical sm-none">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7 col-md-8 col-sm-8">
                    <div class="breadcrumb-title">
                        <h2>Noticias</h2>
                        <ul class="breadcrumb-menu list-style">
                            <li><a href="./index.php?view=home">Inicio </a></li>
                            <li>Noticias</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-5 col-md-4 col-sm-4 xs-none">
                    <div class="breadcrumb-img">
                        <img src="assets/img/breadcrumb/br-shape-5.png" alt="Image" class="br-shape-five animationFramesTwo">
                        <img src="assets/img/breadcrumb/br-shape-6.png" alt="Image" class="br-shape-six bounce">
                        <img src="assets/img/breadcrumb/breadcrumb-4.png" alt="Image">
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Blog Details Section Start -->
    <div class="blog-wrap ptb-100">
        <div class="container">
            <div class="row gx-5">

                <div class="col-xl-8 col-lg-12">

                    <div class="row justify-content-center">
                        <div id="content-news" class="row justify-content-center">
                            Cargando Fracciones...
                        </div>
                    </div>
                    <ul class="page-nav list-style mt-10 pagination page-numbers col-12" id="pagination">
                        <!-- <li>
                            <a href="blog-left-sidebar.html"><i class="flaticon-back"></i></a>
                        </li>
                        <li><a class="active" href="blog-left-sidebar.html">1</a></li>
                        <li><a href="blog-left-sidebar.html">2</a></li>
                        <li><a href="blog-left-sidebar.html">3</a></li>
                        <li><a href="blog-left-sidebar.html"><i class="flaticon-next-1"></i></a></li> -->
                        <!-- <li>
                            <a href="JavaScript:Void(0);" data-num="1" class="flecha"><i class="flaticon-back"></i></a>
                        </li> -->
                        <?php
                        if (!empty($array["count"])) {
                            for ($i = 1; $i <= ceil($array["count"] / 12); $i++) {
                                if ($i <= 6) {
                                    if ($i == 1) { ?>

                                        <li class="page-item" id="p<?php echo $i; ?>">
                                            <a href="JavaScript:Void(0);" data-id="<?php echo $i; ?>" class="page-link active"><?php echo $i; ?></a>
                                        </li>

                                    <?php
                                    } else {
                                    ?>

                                        <li class="page-item" id="p<?php echo $i; ?>">
                                            <a href="#pagination" data-id="<?php echo $i; ?>" class="page-link"><?php echo $i; ?></a>
                                        </li>

                        <?php
                                    }
                                }
                            }
                        }
                        ?>
                        <!-- <li>
                            <a href="JavaScript:Void(0);" data-num="2" class="flecha"><i class="flaticon-next-1"></i></a>
                        </li> -->
                    </ul>
                </div>

                <div class="col-xl-4 col-lg-12">
                    <div class="sidebar">

                        <div class="sidebar-widget style4">
                            <div class="search-box">
                                <div class="form-group">
                                    <input type="search" placeholder="Buscar">
                                    <button type="submit">
                                        <i class="flaticon-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="sidebar-widget categories">
                            <h4>Categorias</h4>
                            <div class="category-box">
                                <ul class="list-style">
                                    <?php $count = 0;
                                    foreach ($departament['departments'] as $d) :
                                        $count++ ?>
                                        <li>
                                            <a href="#">
                                                <i class="ri-login-box-line"></i><?php echo $d['name']; ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                    <!-- <li>
                                        <a href="posts-by-category.html">
                                            <i class="ri-login-box-line"></i>
                                            Business Card
                                            <span>(2)</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="posts-by-category.html">
                                            <i class="ri-login-box-line"></i>
                                            Uncategorized
                                            <span>(6)</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="posts-by-category.html">
                                            <i class="ri-login-box-line"></i>
                                            Online Banking
                                            <span>(5)</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="posts-by-category.html">
                                            <i class="ri-login-box-line"></i>
                                            Digital Payment
                                            <span>(9)</span>
                                        </a>
                                    </li> -->
                                </ul>
                            </div>
                        </div>

                        <div class="sidebar-widget popular-post">
                            <h4>Noticias recientes</h4>
                            <div class="popular-post-widget">
                                <?php $count = 0;
                                foreach ($newslimit['news'] as $n) :
                                    $count++ ?>
                                    <div class="pp-post-item">
                                        <a href="index.php?view=noticia_detallada&id_noticia=<?php echo $n['id'] ?>" class="pp-post-img">
                                            <img loading="lazy" src="<?php echo $GLOBALS['multimedia']."/files/imgNews/".$n['cover_page']; ?>" alt="Image" style="width: 80px;">
                                        </a>
                                        <div class="pp-post-info">
                                            <span><i class="flaticon-calendar"></i>
                                                <?php
                                                $campo = date($n['created_at']);
                                                $date = date_create($campo);
                                                $fecha = date_format($date, 'd-m-Y');
                                                echo $fecha;
                                                ?>
                                            </span>
                                            <h6>
                                                <a href="index.php?view=noticia_detallada&id_noticia=<?php echo $n['id'] ?>">
                                                    <?php echo $n['title']; ?>
                                                </a>
                                            </h6>
                                        </div>
                                    </div>
                                    <!-- <div class="pp-post-item">
                                        <a href="blog-details-right-sidebar.html" class="pp-post-img">
                                            <img src="assets/img/blog/post-thumb-2.jpg" alt="Image">
                                        </a>
                                        <div class="pp-post-info">
                                            <span><i class="flaticon-calendar"></i>25 Sep, 2021</span>
                                            <h6>
                                                <a href="blog-details-no-sidebar.html">
                                                    Available Financial Loans For Business
                                                </a>
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="pp-post-item">
                                        <a href="blog-details-right-sidebar.html" class="pp-post-img">
                                            <img src="assets/img/blog/post-thumb-3.jpg" alt="Image">
                                        </a>
                                        <div class="pp-post-info">
                                            <span><i class="flaticon-calendar"></i>12 Sep, 2021</span>
                                            <h6>
                                                <a href="blog-details-no-sidebar.html">
                                                    5 Benefit Of Credit Card In Payment
                                                </a>
                                            </h6>
                                        </div>
                                    </div> -->
                                <?php endforeach; ?>
                            </div>
                        </div>

                        <!-- <div class="sidebar-widget tags">
                            <h4>Popular Tags </h4>
                            <div class="tag-list">
                                <ul class="list-style">
                                    <li><a href="posts-by-tag.html">Business</a></li>
                                    <li><a href="posts-by-tag.html">Costs</a></li>
                                    <li><a href="posts-by-tag.html">Interest</a></li>
                                    <li><a href="posts-by-tag.html">Loan</a></li>
                                    <li><a href="posts-by-tag.html">Payment</a></li>
                                    <li><a href="posts-by-tag.html">Cards</a></li>
                                </ul>
                            </div>
                        </div> -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Blog Details Section End -->

</div>
<!-- Content Wrapper End -->

<script>
    let pagination = 1;

    $(document).ready(function() {
        $("#content-news").load("./core/controller/paginationNews.php?page=1&max=12&type=1");
    });

    $(".page-link").click(function() {
        var btn = this;
        pagination = $(this).attr("data-id");
        //var l = $('#limite').val();
        //var total_records = $('#total_records').val();
        $.ajax({
            url: "core/controller/paginationNews.php",
            type: "GET",
            data: {
                page: pagination,
                max: 12,
                type: 1,
            },
            //cache: false,
            success: function(dataResult) {
                $('html, body').animate({
                    scrollTop: 100
                }, 100);
                $("#content-news").html(dataResult);
                $(".page-item a").removeClass("active");
                $(btn).addClass(" active");

            }
        });
    });

    $(".flecha").click(function() {
        var btn = this;
        var flecha = $(this).attr("data-num");
        let id;

        if (flecha == 1) {
            if (pagination == 1) {
                alert('desde aqui empiezan las noticias');
            } else {
                id = pagination - 1;
                $.ajax({
                    url: "core/controller/paginationNews.php",
                    type: "GET",
                    data: {
                        page: id,
                        max: 12,
                        type: 1,
                    },
                    //cache: false,
                    success: function(dataResult) {
                        $('html, body').animate({
                            scrollTop: 100
                        }, 100);
                        $("#content-news").html(dataResult);
                        $(".page-item a").removeClass("active");
                        $(btn).addClass(" active");
                        pagination = id;

                    }
                });
            }
        } else {
            id = pagination + 1;
            $.ajax({
                url: "core/controller/paginationNews.php",
                type: "GET",
                data: {
                    page: id,
                    max: 12,
                    type: 1,
                },
                //cache: false,
                success: function(dataResult) {
                    $('html, body').animate({
                        scrollTop: 100
                    }, 100);
                    $("#content-news").html(dataResult);
                    $(".page-item a").removeClass("active");
                    $(btn).addClass(" active");
                    pagination = id;

                }
            });
        }

        /* $.ajax({
            url: "core/controller/paginationNews.php",
            type: "GET",
            data: {
                page: id,
                max: 12,
                type: 1,
            },
            //cache: false,
            success: function(dataResult) {
                $('html, body').animate({
                    scrollTop: 100
                }, 100);
                $("#content-news").html(dataResult);
                $(".page-item a").removeClass("active");
                $(btn).addClass(" active");

            }
        }); */
    })
</script>