<?php
$url = $GLOBALS["api"].'/getAllByAdminandtype?admin=1&type=2';
$json = file_get_contents($url);
$array = json_decode($json, true);

setlocale(LC_TIME, "spanish");

/* echo '<pre>';
print_r($array['fractions']);
var_dump($_GET);
echo '</pre>'; */

?>

<!-- FUNCIÓN ACTIVE MENÚ -->
<script type="text/javascript">
    let elemento = document.getElementById("li-transparencia");
    elemento.className += " active";
    let elemento2 = document.getElementById("li-especificas");
    elemento2.className += " active";
</script>

<!-- Content Wrapper Start -->
<div class="content-wrapper">

    <!-- Breadcrumb Start -->
    <div class="breadcrumb-wrap bg-spring">
        <img src="assets/img/breadcrumb/br-shape-1.png" alt="Image" class="br-shape-one xs-none">
        <img src="assets/img/breadcrumb/br-shape-2.png" alt="Image" class="br-shape-two xs-none">
        <img src="assets/img/breadcrumb/br-shape-3.png" alt="Image" class="br-shape-three moveHorizontal sm-none">
        <img src="assets/img/breadcrumb/br-shape-4.png" alt="Image" class="br-shape-four moveVertical sm-none">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7 col-md-8 col-sm-8">
                    <div class="breadcrumb-title">
                        <h2>Transparencia</h2>
                        <ul class="breadcrumb-menu list-style">
                            <li><a href="index.html">Inicio </a></li>
                            <li>Transparencia</li>
                            <li>Obligaciones Especificas</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-5 col-md-4 col-sm-4 xs-none">
                    <div class="breadcrumb-img">
                        <img src="assets/img/breadcrumb/br-shape-5.png" alt="Image" class="br-shape-five animationFramesTwo">
                        <img src="assets/img/breadcrumb/br-shape-6.png" alt="Image" class="br-shape-six bounce">
                        <img src="assets/img/breadcrumb/breadcrumb-2.png" alt="Image">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Service Section Start -->
    <!-- <section class="service-wrap  ptb-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="service-card text-center style1">
                        <a href="">
                            <span class="service-icon">
                                <img src="assets/img/service/service-icon-1.png" alt="Image">
                            </span>
                            <h3><a href="service-details.html">Marco Normativo</a></h3>
                            <p>Fraccion l</p>
                        </a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="service-card text-center style1">
                        <a href="">
                            <span class="service-icon">
                                <img src="assets/img/service/service-icon-2.png" alt="Image">
                            </span>
                            <h3><a href="service-details.html">Estructura Orgánica Completa</a></h3>
                            <p>Fraccion ll</p>
                        </a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="service-card text-center style1">
                        <a href="">
                            <span class="service-icon">
                                <img src="assets/img/service/service-icon-3.png" alt="Image">
                            </span>
                            <h3><a href="service-details.html">Facultades de las áreas administrativas</a></h3>
                            <p>Fraccion lll</p>
                        </a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="service-card text-center style1">
                        <a href="">
                            <span class="service-icon">
                                <img src="assets/img/service/service-icon-4.png" alt="Image">
                            </span>
                            <h3><a href="service-details.html">Metas y Objetivos de las áreas administrativas</a></h3>
                            <p>Fraccion lv</p>
                        </a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="service-card text-center style1">
                        <a href="">
                            <span class="service-icon">
                                <img src="assets/img/service/service-icon-5.png" alt="Image">
                            </span>
                            <h3><a href="service-details.html">Indicadores de gestión de interés publico o trascendencia social</a></h3>
                            <p>Fraccion v</p>
                        </a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="service-card text-center style1">
                        <a href="">
                            <span class="service-icon">
                                <img src="assets/img/service/service-icon-6.png" alt="Image">
                            </span>
                            <h3><a href="service-details.html">Indicadores de objetivos y resultados</a></h3>
                            <p>Fraccion lv</p>
                        </a>
                    </div>
                </div>
            </div>
            <ul class="page-nav list-style mt-20">
                <li><a href="service-one.html"><i class="flaticon-back"></i></a></li>
                <li><a class="active" href="service-one.html">1</a></li>
                <li><a href="service-one.html">2</a></li>
                <li><a href="service-one.html">3</a></li>
                <li><a href="service-one.html"><i class="flaticon-next-1"></i></a></li>
            </ul>
        </div>
    </section> -->
    <!-- Service Section End -->

    <section class="service-wrap ptb-100">
        <div class="container">
            <div class="row gx-5">

                <h1>OBLIGACIONES DE TRANSPARENCIA ESPECÍFICAS</h1>
                <h3>Artículo 16</h3>
                <p>Además de lo señalado en el artículo anterior, los siguientes sujetos obligados deberán poner a disposición del público y actualizar la siguiente información.</p>

                <div class="col-xl-8 col-lg-12">

                    <div id="target-content" class="row justify-content-center">
                        Cargando Fracciones...
                    </div>

                    <ul class="page-nav list-style mt-20" id="pagination">
                        <?php
                        if (!empty($array["count"])) {
                            for ($i = 1; $i <= ceil($array["count"] / 12); $i++) {
                                if ($i == 1) { ?>
                                    <li class="page-item" id="p<?php echo $i; ?>">
                                        <a href="JavaScript:Void(0);" data-id="<?php echo $i; ?>" class=" current page-link"><?php echo $i; ?></a>
                                    </li>
                                <?php
                                } else {
                                ?>
                                    <li class="page-item" id="p<?php echo $i; ?>">
                                        <a href="JavaScript:Void(0);" class=" page-link" data-id="<?php echo $i; ?>"><?php echo $i; ?></a>
                                    </li>
                        <?php
                                }
                            }
                        }
                        ?>
                    </ul>

                </div>

                <div class="col-xl-4 col-lg-12">
                    <div class="sidebar">

                        <div class="sidebar-widget categories">
                            <span>Vista</span>
                            <h4>Transparencia 2022 - 2025</h4>
                            <div class="category-box">
                                <ul class="list-style">
                                    <li class="cat-item"><a href="./index.php?view=transparencia_obligaciones_comunes"><i class="ri-login-box-line"></i>Ley 875 Art 15 (Obligaciones Comúnes)</a></li>
                                    <li class="cat-item"><a href="./index.php?view=transparencia_obligaciones_especificas"><i class="ri-login-box-line"></i>Ley 875 Art 16 (Obligaciones Especificas)</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="sidebar-widget categories">
                            <span>Portal de</span>
                            <h4 class="widget-title"><span>Transparencia ciudadana</span></h4>
                            <p>Conoce la Ley General de Contabilidad Gubernamental.</p>
                            <div class="category-box">
                                <a href="https://www.plataformadetransparencia.org.mx/" target="_blank" class="btn style1"><span>VISITAR PORTAL</span></a>
                            </div>
                        </div>

                        <div class="sidebar-widget categories">
                            <span>Descarga</span>
                            <h4 class="widget-title"><span>Leyes de Transparencia</span></h4>
                            <div class="category-box">
                                <ul class="list-style">
                                    <li class="cat-item"><a href="https://www.diputados.gob.mx/LeyesBiblio/pdf/LGTAIP_200521.pdf" title="" target="_blank"><i class="ri-login-box-line"></i>Ley General de Transparencia</a></li>
                                    <li class="cat-item"><a href="https://www.diputados.gob.mx/LeyesBiblio/pdf/LGCG_300118.pdf" title="" target="_blank"><i class="ri-login-box-line"></i>Ley General de Archivos</a></li>
                                    <li class="cat-item"><a href="https://www.diputados.gob.mx/LeyesBiblio/pdf/LGPDPPSO.pdf" title="" target="_blank"><i class="ri-login-box-line"></i>Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados</a></li>
                                    <li class="cat-item"><a href="https://www.legisver.gob.mx/leyes/LeyesPDF/TRANSPARENCIA290916.pdf" title="" target="_blank"><i class="ri-login-box-line"></i>Ley 875 de Transparencia y Acceso a la Información Pública para el Estado de Veracruz</a></li>
                                    <li class="cat-item"><a href="https://www.diputados.gob.mx/LeyesBiblio/pdf/LGPDPPSO.pdf" title="" target="_blank"><i class="ri-login-box-line"></i>Ley Protección de Datos Personales en Posesión de Sujetos Obligados para Estado de Veracruz</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section><!-- .col-md-9 -->

</div>
<!-- Content Wrapper End -->

<script>
    $(document).ready(function() {
        $("#target-content").load("./core/controller/pagination.php?page=1&max=12&type=2");

        $(".page-link").click(function() {
            var id = $(this).attr("data-id");
            //var l = $('#limite').val();
            //var total_records = $('#total_records').val();
            $.ajax({
                url: "core/controller/pagination.php",
                type: "GET",
                data: {
                    page: id,
                    max: 12,
                    type: 2,
                },
                //cache: false,
                success: function(dataResult) {
                    $("#target-content").html(dataResult);
                    $(".page-item").removeClass("active");
                    $(this).addClass("current");

                }
            });
        });

        $("#portfolio-flters li").click(function() {

            var id = $(this).data("id");
            var select_id = $('.page-item .active').data("id");
            var l = $('#limite').val();
            $.ajax({
                url: "./core/controller/pagination.php",
                type: "GET",
                data: {
                    filtro: id,
                    l: l,
                    type: 2,
                },
                cache: false,
                success: function(dataResult) {
                    $("#target-content").html(dataResult);
                    $(".page-item").removeClass("active");
                    $("#" + select_id).addClass("active");
                    var pagin = '';
                    var pags = $('#total_pages').val();
                    for (let index = 1; index <= pags; index++) {
                        pagin += '<li class="page-item active" id="' + index + '"><button data-id="' + index + '" class="page-link">' + index + '</button></li>'

                    }
                    $('#pagination').html(pagin);

                }
            });
        });
    });
</script>