<section id="coming-soon" class="coming-sec">
    <div class="container-fluid">
        <div class="row">
            <ul class="page_decore">
                <li> <img src="assets/img/coming_soon/1.png" alt=""/> </li>
                <li> <img src="assets/img/coming_soon/2.png" alt=""/> </li>
                <li> <img src="assets/img/coming_soon/3.png" alt=""/> </li>
            </ul>
            <div class="col-lg-7 offset-lg-5">
                <div class="wrapper">
                    <div>
                        <div class="content">
                            <h1>¡Próximamente, en breve, pronto!</h1>
                            <div class="countdown">
                                <div class="countdown__days">
                                    <div class="number">1</div>
                                    <span class>Dias</span>
                                </div>
                                <div class="countdown__hours">
                                    <div class="number">0</div>
                                    <span class>Horas</span>
                                </div>
                                <div class="countdown__minutes">
                                    <div class="number">0</div>
                                    <span class>Minutos</span>
                                </div>
                                <div class="countdown__seconds">
                                    <div class="number">0</div>
                                    <span class>Segundos</span>
                                </div>
                            </div>
                            <div class="form-text">
                                <div class="text">
                                    <p>Nuestro sitio web está en construcción. Estaremos aquí pronto con nuestro nuevo sitio impresionante.</p>
                                </div>
                                <!-- <form class="coming-soon">
                                    <div class="form-group">
                                        <input id="name" class="input-text js-input" type="text" required>
                                    </div>
                                    <a class="btn_rounded" href="#" >Enviar</a>
                                </form> -->
                            </div>
                            <div class="social">
                                <ul class="footer__links">
                                    <li><a href="https://www.facebook.com/municipiomixtequilla" target="_blank"><span class="fa fa-facebook"></span></a></li>
                                    <!-- <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-instagram"></span></a></li>
                                    <li><a href="#"><span class="fa fa-telegram"></span></a></li> -->
                                    <li><a href="https://wa.me/522859760017?text=Hola!%20requiero%20información!" target="_blank"><span class="fa fa-whatsapp"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>