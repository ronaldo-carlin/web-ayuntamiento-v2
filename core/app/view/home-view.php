<?php
$url = $GLOBALS["api"] . '/getNewsPublic';
$json = file_get_contents($url);
$array = json_decode($json, true);

$url2 = $GLOBALS["api"] . '/getDepartments';
$json2 = file_get_contents($url2);
$departament = json_decode($json2, true);

$url3 = $GLOBALS["api"] . '/getAllNewsLimit';
$json3 = file_get_contents($url3);
$newslimit = json_decode($json3, true);

setlocale(LC_TIME, "spanish");

/* echo '<pre>';
print_r($array['fractions']);
var_dump($_GET);
echo '</pre>'; */

?>

<!-- FUNCIÓN ACTIVE MENÚ -->
<script type="text/javascript">
    var elemento = document.getElementById("li-home");
    elemento.className += " active";
</script>

<!-- portada Start -->
<section class="hero-wrap style3 bg-squeeze">
    <img src="assets/img/hero/hero-shape-10.png" alt="Image" class="hero-shape-one">
    <img src="assets/img/hero/hero-shape-15.png" alt="Image" class="hero-shape-two">
    <img src="assets/img/hero/hero-shape-14.png" alt="Image" class="hero-shape-three">
    <img src="assets/img/hero/hero-shape-11.png" alt="Image" class="hero-shape-four animationFramesTwo">
    <div class="hero-slider-one owl-carousel">

        <div class="hero-slide-item">
            <div class="container">
                <div class="row gx-5 align-items-center">
                    <div class="col-md-6">
                        <div class="hero-content" data-speed="0.10" data-revert="true">
                            <!-- <span>Instant Money Transfer</span> -->
                            <h1>Bienvenido!</h1>
                            <!-- <p>Dea of denouncing pleasure and praising pain was born and lete system, and expound the ac teachings aitems to sed quia non numquam amet sit dolor.</p> -->
                            <div class="hero-btn">
                                <a href="#tel-eme" class="btn style1">Empezar</a>
                                <a class="play-video" data-fancybox href="https://youtu.be/LfnAKcDSaVY">
                                    <span class="play-btn"> <i class="flaticon-play-button-arrowhead"></i></span>
                                    <span class="sm-none">Ver Video</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="hero-img-wrap">
                            <img src="assets/img/hero/hero-shape-13.png" alt="Image" class="hero-shape-five bounce">
                            <img src="assets/img/hero/hero-shape-12.png" alt="Image" class="hero-shape-six moveHorizontal">
                            <img src="assets/img/hero/hero-slide-1.png" alt="Image" class="hero-img">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="hero-slide-item">
            <div class="container">
                <div class="row gx-5 align-items-center">
                    <div class="col-md-6">
                        <div class="hero-content" data-speed="0.10" data-revert="true">
                            <!-- <span>Instant Money Transfer</span> -->
                            <h1>Bienvenido!</h1>
                            <!-- <p>Dea of denouncing pleasure and praising pain was born and lete system, and expound the ac teachings aitems to sed quia non numquam amet sit dolor.</p> -->
                            <div class="hero-btn">
                                <a href="#tel-eme" class="btn style1">Empezar</a>
                                <a class="play-video" data-fancybox href="https://youtu.be/LfnAKcDSaVY">
                                    <span class="play-btn"> <i class="flaticon-play-button-arrowhead"></i></span>
                                    <span class="sm-none">Ver Video</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="hero-img-wrap">
                            <img src="assets/img/hero/hero-shape-13.png" alt="Image" class="hero-shape-five bounce">
                            <img src="assets/img/hero/hero-shape-12.png" alt="Image" class="hero-shape-six moveHorizontal">
                            <img src="assets/img/hero/hero-slide-2.png" alt="Image" class="hero-img">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="hero-slide-item">
            <div class="container">
                <div class="row gx-5 align-items-center">
                    <div class="col-md-6">
                        <div class="hero-content" data-speed="0.10" data-revert="true">
                            <!-- <span>Instant Money Transfer</span> -->
                            <h1>Bienvenido!</h1>
                            <!-- <p>Dea of denouncing pleasure and praising pain was born and lete system, and expound the ac teachings aitems to sed quia non numquam amet sit dolor.</p> -->
                            <div class="hero-btn">
                                <a href="#tel-eme" class="btn style1">Empezar</a>
                                <a class="play-video" data-fancybox href="https://youtu.be/LfnAKcDSaVY">
                                    <span class="play-btn"> <i class="flaticon-play-button-arrowhead"></i></span>
                                    <span class="sm-none">Ver Video</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="hero-img-wrap">
                            <img src="assets/img/hero/hero-shape-13.png" alt="Image" class="hero-shape-five bounce">
                            <img src="assets/img/hero/hero-shape-12.png" alt="Image" class="hero-shape-six moveHorizontal">
                            <img src="assets/img/hero/hero-slide-3.png" alt="Image" class="hero-img">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- portada End -->

<!-- numeros de emergencia Start -->
<section class="feature-wrap pt-100 pb-75" id="tel-eme">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="feature-card style3">
                    <a href="#">
                        <div class="feature-info">
                            <div class="feature-title">
                                <!-- <span><img src="assets/img/feature/feature-icon-1.png" alt="Image"></span> -->
                                <span class="">
                                    <i class="bi bi-people"></i>
                                </span>
                                <h3>Atención Ciudadana</h3>
                            </div>
                            <!-- <p>Lorem ipsum dolor sit amet dolor alut const slice elit aliquid dolor ametin perfer endis velit sed fuga volup tation sit praising pain.</p> -->
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="feature-card style3">
                    <a href="tel:2859760017">
                        <div class="feature-info">
                            <div class="feature-title">
                                <!-- <span><img src="assets/img/feature/feature-icon-2.png" alt="Image"></span> -->
                                <span class="">
                                    <i class="bi bi-telephone-outbound"></i>
                                </span>
                                <div class="w-80 px-4">
                                    <h3 class="m-0 w-100">Atención teléfonica</h3>
                                    <p>Municipio de ignacio de la llave</p>
                                    <p><samp>(285) 976 0017</samp></p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="feature-card style3">
                    <a href="#">
                        <div class="feature-info">
                            <div class="feature-title">
                                <!-- <span><img src="assets/img/feature/feature-icon-3.png" alt="Image"></span> -->
                                <span class="">
                                    <i class="bi bi-telephone-plus"></i>
                                </span>
                                <h3>Números de <samp>EMERGENCIA</samp></h3>
                            </div>
                            <!-- <p>Lorem ipsum dolor sit amet dolor alut const slice elit aliquid dolor ametin perfer endis velit sed fuga volup tation sit praising pain.</p> -->
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- numeros de emergencia End -->

<!-- servicios municipales Start -->
<section class="why-choose-wrap style1 pb-100 bg-bunting">
    <div class="container">
        <div class="row gx-5 align-items-center">
            <div class="col-lg-6">
                <div class="wh-img-wrap">
                    <img src="assets/img/why-choose-us/wh-img-1.png" alt="Image">
                    <img class="wh-shape-one bounce" src="assets/img/why-choose-us/wh-shape-1.png" alt="Image">
                    <img class="wh-shape-two animationFramesTwo" src="assets/img/why-choose-us/wh-shape-2.png" alt="Image">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="wh-content">
                    <div class="content-title style1">
                        <!-- <span>Why Choose Us</span> -->
                        <h2>¡Bienvenidos!</h2>
                        <p>Bienvenidas y bienvenidos a la página digital del H. Ayuntamiento de Ignacio de la llave, en donde podrás consultar la información relacionada con trámites y servicios municipales. También encontrarás las últimas noticias de las acciones que emprendemos para beneficiar a Ignacio de la llave. <br>
                            Consulta nuestro apartado de transparencia, y recuerda que siempre estamos cercano a ti.</p>
                    </div>
                    <!-- <div class="feature-item-wrap">
                        <div class="feature-item">
                            <span class="feature-icon">
                                <i class="flaticon-graph"></i>
                            </span>
                            <div class="feature-text">
                                <h3>Low Costing</h3>
                                <p>Vestibulum ac diam sit amet quam vehicula elemen tum sed sit amet dui praesent sapien pelle tesque.</p>
                            </div>
                        </div>
                        <div class="feature-item">
                            <span class="feature-icon">
                                <i class="flaticon-loan-1"></i>
                            </span>
                            <div class="feature-text">
                                <h3>Safe &amp; Secure</h3>
                                <p>Vestibulum ac diam sit amet quam vehicula elemen tum sed sit amet dui praesent sapien pelle tesque.</p>
                            </div>
                        </div>
                        <div class="feature-item">
                            <span class="feature-icon">
                                <i class="flaticon-computer"></i>
                            </span>
                            <div class="feature-text">
                                <h3>Live Support</h3>
                                <p>Vestibulum ac diam sit amet quam vehicula elemen tum sed sit amet dui praesent sapien pelle tesque.</p>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- servicios municipales End -->

<!-- servicios a la comunidad start -->
<div class="counter-wrap">
    <div class="container">
        <div class="counter-card-wrap">
            <div class="counter-card">
                <div class="counter-text">
                    <div class="counter-num">
                        <span class="odometer" data-count="17105"></span>
                    </div>
                    <p>Ciudadanos</p>
                </div>
            </div>
            <div class="counter-card">
                <div class="counter-text">
                    <div class="counter-num">
                        <span class="odometer" data-count="59"></span>
                    </div>
                    <p>Pueblos</p>
                </div>
            </div>
            <div class="counter-card">
                <div class="counter-text">
                    <div class="counter-num">
                        <span class="odometer" data-count="35500"></span>
                    </div>
                    <p>Densidad de población del municipio de Ignacio de la Llave</p>
                </div>
            </div>
            <div class="counter-card">
                <div class="counter-text">
                    <div class="counter-num">
                        <span class="" data-count="">95240</span>
                    </div>
                    <p>Codigo postal</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- servicios a la comunidad End -->

<!-- servicios municipales Start -->
<section class="service-wrap style3 ptb-100 bg-rock">
    <div class="container">
        <img src="assets/img/service-shape-1.png" alt="Image" class="service-shape-one">
        <img src="assets/img/service-shape-2.png" alt="Image" class="service-shape-two">
        <div class="section-title style1 text-center mb-40">
            <!-- <span>Our Services</span> -->
            <h2 class="text-white">Servicios Municipales</h2>
        </div>
        <div class="row gx-5 align-items-center">
            <div class="col-md-6">
                <div class="service-card style3">
                    <span class="service-icon">
                        <i class="bi bi-droplet"></i>
                    </span>
                    <div class="service-info">
                        <h3><a href="service-details.html">Agua</a></h3>
                        <p>Is simply dummy text of the printing and setting type the industry's standard dummy text ever since then nowpri nter took a galley alto.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="service-card style3">
                    <span class="service-icon">
                        <i class="bi bi-lightbulb"></i>
                    </span>
                    <div class="service-info">
                        <h3><a href="service-details.html">Alumbrado</a></h3>
                        <p>Is simply dummy text of the printing and setting type the industry's standard dummy text ever since then nowpri nter took a galley alto.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="service-card style3">
                    <span class="service-icon">
                        <i class="bi bi-house"></i>
                    </span>
                    <div class="service-info">
                        <h3><a href="service-details.html">Cementerios</a></h3>
                        <p>Is simply dummy text of the printing and setting type the industry's standard dummy text ever since then nowpri nter took a galley alto.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="service-card style3">
                    <span class="service-icon">
                        <i class="bi bi-trash3"></i>
                    </span>
                    <div class="service-info">
                        <h3><a href="service-details.html">Limpia Publica</a></h3>
                        <p>Is simply dummy text of the printing and setting type the industry's standard dummy text ever since then nowpri nter took a galley alto.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="service-card style3">
                    <span class="service-icon">
                        <i class="bi bi-flower1"></i>
                    </span>
                    <div class="service-info">
                        <h3><a href="service-details.html">Parques y Jardines</a></h3>
                        <p>Is simply dummy text of the printing and setting type the industry's standard dummy text ever since then nowpri nter took a galley alto.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="service-card style3">
                    <span class="service-icon">
                        <i class="bi bi-heart-pulse"></i>
                    </span>
                    <div class="service-info">
                        <h3><a href="service-details.html">Salud</a></h3>
                        <p>Is simply dummy text of the printing and setting type the industry's standard dummy text ever since then nowpri nter took a galley alto.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="text-center mt-20">
            <a href="#tel-eme" class="btn style1">View All Services</a>
        </div> -->
    </div>
</section>
<!-- servicios municipales End -->

<!-- servicios a la comunidad start -->
<section class="feature-wrap pt-100 pb-75">
    <div class="container">
        <div class="section-title style1 text-center mb-40">
            <!-- <span>Our Services</span> -->
            <h2 class="">Servicios a la comunidad</h2>
        </div>
        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner p-4">
                <div class="carousel-item px-5 active">
                    <div class="row justify-content-center">
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="feature-card style3">
                                <a href="https://www.gob.mx/curp/" target="_blank">
                                    <div class="feature-info text-center">
                                        <div class="feature-title justify-content-center flex-column">
                                            <img src="assets/img/Community Services/curp.png" alt="Image">
                                            <h3>Consulta tu CURP</h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="feature-card style3">
                                <a href="https://www.ovh.gob.mx/" target="_blank">
                                    <div class="feature-info text-center">
                                        <div class="feature-title justify-content-center flex-column">
                                            <img src="assets/img/Community Services/ovh.png" alt="Image">
                                            <h3>Oficina Virtual de Hacienda del Estado de Veracruz(OVH)</h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="feature-card style3">
                                <a href="https://www.gob.mx/actas" target="_blank">
                                    <div class="feature-info text-center">
                                        <div class="feature-title justify-content-center flex-column">
                                            <img src="assets/img/Community Services/acta de nacimiento.jpg" alt="Image">
                                            <h3>Acta de Nacimiento en Línea</h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item px-5">
                    <div class="row justify-content-center">
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="feature-card style3">
                                <a href="https://www.gob.mx/tramites/ficha/expedicion-de-la-credencial-inapam/INAPAM2791" target="_blank">
                                    <div class="feature-info text-center">
                                        <div class="feature-title justify-content-center flex-column">
                                            <img src="assets/img/Community Services/inapam.png" alt="Image">
                                            <h3>INAPAM</h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="feature-card style3">
                                <a href="https://cvcovid.salud.gob.mx/" target="_blank">
                                    <div class="feature-info text-center">
                                        <div class="feature-title justify-content-center flex-column">
                                            <img src="assets/img/Community Services/covid.png" alt="Image">
                                            <h3>Certificado de vacunación COVID-19</h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="feature-card style3">
                                <a href="https://www.gob.mx/pasaporte" target="_blank">
                                    <div class="feature-info text-center">
                                        <div class="feature-title justify-content-center flex-column">
                                            <img src="assets/img/Community Services/pasaporte.jpg" alt="Image">
                                            <h3>Pasaporte en línea</h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
</section>
<!-- servicios a la comunidad End -->

<!-- noticias Start -->
<section class="blog-wrap pt-100 pb-75 bg-albastor">
    <div class="container">
        <div class="section-title style1 text-center mb-40">
            <!-- <span>Latest Insights From Raxa</span> -->
            <h2>Noticias</h2>
        </div>
        <div class="row justify-content-center">
            <div id="content-news" class="row justify-content-center">
                Cargando Fracciones...
            </div>
        </div>
        <!-- <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="blog-card style1">
                    <div class="blog-img">
                        <img src="assets/img/blog/blog-1.jpg" alt="Image">
                    </div>
                    <div class="blog-info">

                        <ul class="blog-metainfo  list-style">
                            <li><i class="flaticon-user"></i> <a href="posts-by-author.html">Reanne Carnation</a></li>
                            <li><i class="flaticon-calendar"></i>02 Jan, 2022</li>
                        </ul>
                        <h3><a href="blog-details-right-sidebar.html">5 Things To Know About Your Online Banking</a></h3>
                        <a href="blog-details-right-sidebar.html" class="link style1">Read More
                            <i class="flaticon-right-arrow"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="blog-card style1">
                    <div class="blog-img">
                        <img src="assets/img/blog/blog-2.jpg" alt="Image">
                    </div>
                    <div class="blog-info">
                        <ul class="blog-metainfo  list-style">
                            <li><i class="flaticon-user"></i> <a href="posts-by-author.html">Phil Heath</a></li>
                            <li><i class="flaticon-calendar"></i>12 Dec, 2021</li>
                        </ul>
                        <h3><a href="blog-details-right-sidebar.html">Tips For Saving Money &amp; Better Investment Policy</a></h3>
                        <a href="blog-details-right-sidebar.html" class="link style1">Read More
                            <i class="flaticon-right-arrow"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="blog-card style1">
                    <div class="blog-img">
                        <img src="assets/img/blog/blog-3.jpg" alt="Image">
                    </div>
                    <div class="blog-info">
                        <ul class="blog-metainfo  list-style">
                            <li><i class="flaticon-user"></i><a href="posts-by-author.html">Phil Heath</a></li>
                            <li><i class="flaticon-calendar"></i>12 Dec, 2021</li>
                        </ul>
                        <h3><a href="blog-details-right-sidebar.html">The Fedaral Bank Appoints New Director To The Board</a></h3>

                        <a href="blog-details-right-sidebar.html" class="link style1">Read More
                            <i class="flaticon-right-arrow"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</section>
<!-- noticias End -->

<script>
    $(document).ready(function() {
        $("#content-news").load("./core/controller/paginationNews.php?page=1&max=6&type=2");
    });
</script>