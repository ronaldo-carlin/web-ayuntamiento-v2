<?php
$id = ($_GET['id_fraction']);
$url_fraction_id = $GLOBALS["api"].'/getFractionById?id=' . $id;
echo $url_fraction_id;
$json = file_get_contents($url_fraction_id);
$array = json_decode($json, true);

$url_section = $GLOBALS["api"].'/getAllByFraction?fraction=' . $id;
$json_section = file_get_contents($url_section);
$array_section = json_decode($json_section, true);

$count2 = 0;
foreach ($array_section['sections'] as $n) {
    $count2++;
    if ($count2 == 1) {
        $id_section = $n['id'];
    }
}

$url_period = $GLOBALS["api"].'/getAllBySubsection?section=' . $id_section;
$json_period = file_get_contents($url_period);
$array_period = json_decode($json_period, true);

/* $id_section = $array_section['sections'][0]['id'];
$url_period = $GLOBALS["api"].'/getAllByPeriod?section_id=' . $id_section;
 */

/* echo '<pre>';
print_r($id_section);
echo '</pre>'; */

/* FECHA */
$mes = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
//fecha de validacion
$mesv = date('n', strtotime($array['fraction'][0]['date_validation']));
$diav = date('d', strtotime($array['fraction'][0]['date_validation']));
$aniov = date('Y', strtotime($array['fraction'][0]['date_validation']));
//fecha de actualizacion
$mesa = date('n', strtotime($array['fraction'][0]['update_date']));
$diaa = date('d', strtotime($array['fraction'][0]['update_date']));
$anioa = date('Y', strtotime($array['fraction'][0]['update_date']));
setlocale(LC_TIME, "spanish");

/* echo '<pre>';
var_dump($_GET);
print_r($array['fraction']);
echo '</pre>'; */
?>

<!-- FUNCIÓN ACTIVE MENÚ -->
<script type="text/javascript">
    let elemento = document.getElementById("li-transparencia");
    elemento.className += " active";
</script>

<!-- FUNCIÓN ACTIVE A.SOLUCIONES MENÚ -->
<!-- <script type="text/javascript">
    var elemento = document.getElementById("li-transparencia");
    elemento.className += " current-menu-item";
</script> -->

<!-- Content Wrapper Start -->
<div class="content-wrapper">

    <!-- Breadcrumb Start -->
    <div class="breadcrumb-wrap bg-spring">
        <img src="assets/img/breadcrumb/br-shape-1.png" alt="Image" class="br-shape-one xs-none">
        <img src="assets/img/breadcrumb/br-shape-2.png" alt="Image" class="br-shape-two xs-none">
        <img src="assets/img/breadcrumb/br-shape-3.png" alt="Image" class="br-shape-three moveHorizontal sm-none">
        <img src="assets/img/breadcrumb/br-shape-4.png" alt="Image" class="br-shape-four moveVertical sm-none">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7 col-md-8 col-sm-8">
                    <div class="breadcrumb-title">
                        <h2>Transparencia</h2>
                        <ul class="breadcrumb-menu list-style">
                            <li><a href="index.html">Inicio </a></li>
                            <li><a href="service-one.html">Transparencia </a></li>
                            <li>Transparencia detallado</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-5 col-md-4 col-sm-4 xs-none">
                    <div class="breadcrumb-img">
                        <img src="assets/img/breadcrumb/br-shape-5.png" alt="Image" class="br-shape-five animationFramesTwo">
                        <img src="assets/img/breadcrumb/br-shape-6.png" alt="Image" class="br-shape-six bounce">
                        <img src="assets/img/breadcrumb/breadcrumb-1.png" alt="Image">
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- Breadcrumb End -->

    <div id="main-content" class="site-main clearfix py-5">
        <div id="content-wrap">
            <div class="site-content clearfix">
                <div id="inner-content" class="inner-content-wrap">
                    <div class="portfolio-details">
                        <div class="container">
                            <div class="row p-4">

                                <!-- secciones -->
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <?php $count = 0;
                                    foreach ($array_section['sections'] as $n) : $count++;
                                        if ($count == 1) :
                                    ?>
                                            <!-- <div id="id_section" style="visibility:hidden;"><?php echo $n['name']; ?></div> -->
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link active" id="section-<?php echo $n['name']; ?>-tab" data-name="<?php echo $n['name']; ?>" data-section_id="<?php echo $n['id']; ?>" onclick="period(this)" data-bs-toggle="tab" data-bs-target="#body-<?php echo $n['name']; ?>" type="button" role="tab" aria-controls="body-<?php echo $n['name']; ?>" aria-selected="true">
                                                    <?php echo $n['name']; ?>
                                                </button>
                                            </li>
                                        <?php else : ?>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="section-<?php echo $n['name']; ?>-tab" data-name="<?php echo $n['name']; ?>" data-section_id="<?php echo $n['id']; ?>" onclick="period(this)" data-bs-toggle="tab" data-bs-target="#body-<?php echo $n['name']; ?>" type="button" role="tab" aria-controls="body-<?php echo $n['name']; ?>" aria-selected="true">
                                                    <?php echo $n['name']; ?>
                                                </button>
                                            </li>

                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </ul>

                                <div class="tab-content p-0">

                                    <?php
                                    if ($array['count'] > 0) :
                                        $count = 0;
                                        if ($array_section['count'] > 0) :
                                            foreach ($array_section['sections'] as $n) :
                                                $count++;
                                                if ($count == 1) : ?>

                                                    <div class="tab-pane active" id="body-<?php echo $n['name']; ?>" role="tabpanel" aria-labelledby="section-<?php echo $n['name']; ?>-tab">

                                                        <div class="d-flex flex-wrap">
                                                            <!-- descripcion de la fraccion -->
                                                            <div class="col-12 col-md-8">
                                                                <div class="cbr-content-box">
                                                                    <div class="inner">

                                                                        <div class="d-flex">
                                                                            <div class="my-3 w-100">
                                                                                <select name="period" id="period<?php echo $n['id'] ?>" class="form-select h-100" aria-label="Default select example" onchange="ShowSelected(this);">
                                                                                    <option value="" disabled selected>Selecciona un Periodo</option>
                                                                                    <?php foreach ($array_period['sections'] as $c) : ?>
                                                                                        <option value="<?php echo $c['id']; ?>"><?php echo $c['name'] ?></option>
                                                                                    <?php endforeach; ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="cbr-headings style-3">
                                                                            <h1 class="heading"><?php echo $array['fraction'][0]['name']; ?></h1>
                                                                        </div><!-- .cbr-headings -->

                                                                        <div class="cbr-spacer clearfix" data-desktop="20" data-mobi="15" data-smobi="15"></div>

                                                                        <div class="cbr-text">
                                                                            <p class="line-height-30"><?php echo $array['fraction'][0]['description']; ?></p>

                                                                            <div class="cbr-spacer clearfix" data-desktop="28" data-mobi="0" data-smobi="0"></div>

                                                                            <div class="post-meta-option">
                                                                                <div class="row gx-0 align-items-center">
                                                                                    <div class="col-md-7 col-12">
                                                                                        <div class="post-tag">
                                                                                            <span> <i class="flaticon-supermarket"></i>Tipo de Fracción:</span>
                                                                                            <ul class="tag-list list-style">
                                                                                                <li><a href="#"><?php echo $array['fraction'][0]['type']; ?></a></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-5 col-12 text-md-end text-start">
                                                                                        <div class="post-share w-100">
                                                                                            <span>Siguenos en:</span>
                                                                                            <ul class="social-profile style2 list-style">
                                                                                                <li>
                                                                                                    <a target="_blank" href="https://www.facebook.com/municipiomixtequilla">
                                                                                                        <i class="ri-facebook-fill"></i>
                                                                                                    </a>
                                                                                                </li>
                                                                                                <!-- <li>
                                                                                                    <a target="_blank" href="https://twitter.com">
                                                                                                        <i class="ri-twitter-fill"></i>
                                                                                                    </a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a target="_blank" href="https://linkedin.com">
                                                                                                        <i class="ri-linkedin-fill"></i>
                                                                                                    </a>
                                                                                                </li> -->
                                                                                                <li>
                                                                                                    <a target="_blank" href="https://www.instagram.com/municipiomixtequilla/">
                                                                                                        <i class="ri-instagram-fill"></i>
                                                                                                    </a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div><!-- .text-wrap -->

                                                                    </div>
                                                                </div><!-- .cbr-content-box -->
                                                            </div><!-- .col-md-8 -->

                                                            <!-- elementos a descargar -->
                                                            <div class="col-12 col-md-4">
                                                                <div class="cbr-spacer clearfix" data-desktop="15" data-mobi="0" data-smobi="0"></div>

                                                                <div class="cbr-content-box gray-bg">
                                                                    <div class="inner">

                                                                        <div class="infor-wrap clearfix">
                                                                            <ul id="descarga" class="project-infor text-center d-flex flex-column justify-content-center p-5"></ul><!-- .project-infor -->
                                                                        </div>

                                                                    </div>
                                                                </div><!-- .cbr-content-box -->

                                                                <div class="cbr-spacer clearfix" data-desktop="0" data-mobi="35" data-smobi="35"></div>
                                                            </div><!-- .col-md-4 -->

                                                        </div>

                                                    </div>

                                                <?php else : ?>

                                                    <div class="tab-pane" id="body-<?php echo $n['name']; ?>" role="tabpanel" aria-labelledby="section-<?php echo $n['name']; ?>-tab">

                                                        <div class="d-flex flex-wrap">
                                                            <!-- descripcion de la fraccion -->
                                                            <div class="col-12 col-md-8">
                                                                <div class="cbr-content-box">
                                                                    <div class="inner">

                                                                        <div id="form_period">
                                                                            <div class="mx-2 my-3 w-100">
                                                                                <select name="period" id="period<?php echo $n['id'] ?>" class="form-select h-100" aria-label="Default select example" onchange="ShowSelected(this);">
                                                                                    <option value="" disabled selected>Selecciona un Periodo</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="cbr-headings style-3">
                                                                            <h2 class="heading"><?php echo $array['fraction'][0]['name']; ?></h2>
                                                                        </div><!-- .cbr-headings -->

                                                                        <div class="cbr-spacer clearfix" data-desktop="20" data-mobi="15" data-smobi="15"></div>

                                                                        <div class="cbr-text">
                                                                            <p class="line-height-30"><?php echo $array['fraction'][0]['description']; ?></p>

                                                                            <div class="cbr-spacer clearfix" data-desktop="28" data-mobi="0" data-smobi="0"></div>

                                                                            <div class="post-meta-option">
                                                                                <div class="row gx-0 align-items-center">
                                                                                    <div class="col-md-7 col-12">
                                                                                        <div class="post-tag">
                                                                                            <span> <i class="flaticon-supermarket"></i>Tipo de Fracción:</span>
                                                                                            <ul class="tag-list list-style">
                                                                                                <li><a href="#"><?php echo $array['fraction'][0]['type']; ?></a></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-5 col-12 text-md-end text-start">
                                                                                        <div class="post-share w-100">
                                                                                            <span>Siguenos en:</span>
                                                                                            <ul class="social-profile style2 list-style">
                                                                                                <li>
                                                                                                    <a target="_blank" href="https://www.facebook.com/municipiomixtequilla">
                                                                                                        <i class="ri-facebook-fill"></i>
                                                                                                    </a>
                                                                                                </li>
                                                                                                <!-- <li>
                                                                                                    <a target="_blank" href="https://twitter.com">
                                                                                                        <i class="ri-twitter-fill"></i>
                                                                                                    </a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a target="_blank" href="https://linkedin.com">
                                                                                                        <i class="ri-linkedin-fill"></i>
                                                                                                    </a>
                                                                                                </li> -->
                                                                                                <li>
                                                                                                    <a target="_blank" href="https://www.instagram.com/municipiomixtequilla/">
                                                                                                        <i class="ri-instagram-fill"></i>
                                                                                                    </a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div><!-- .text-wrap -->

                                                                    </div>
                                                                </div><!-- .cbr-content-box -->
                                                            </div><!-- .col-md-8 -->

                                                            <!-- elementos a descargar -->
                                                            <div class="col-12 col-md-4">
                                                                <div class="cbr-spacer clearfix" data-desktop="15" data-mobi="0" data-smobi="0"></div>

                                                                <div class="cbr-content-box gray-bg">
                                                                    <div class="inner">

                                                                        <div class="infor-wrap clearfix">
                                                                            <ul id="descarga<?php echo $count; ?>" class="project-infor text-center d-flex flex-column justify-content-center p-5"></ul><!-- .project-infor -->
                                                                        </div>

                                                                    </div>
                                                                </div><!-- .cbr-content-box -->

                                                                <div class="cbr-spacer clearfix" data-desktop="0" data-mobi="35" data-smobi="35"></div>
                                                            </div><!-- .col-md-4 -->
                                                        </div>

                                                    </div>


                                            <?php endif;
                                            endforeach;
                                        else : ?>

                                            <div class="tab-pane active" id="body-<?php echo $n['name']; ?>" role="tabpanel" aria-labelledby="section-<?php echo $n['name']; ?>-tab">

                                                <div class="">
                                                    <!-- descripcion de la fraccion -->
                                                    <div class="col-md-8">
                                                        <div class="cbr-content-box" data-padding="15px 0 0 8%" data-mobipadding="0">
                                                            <div class="inner">

                                                                <div class="cbr-headings style-3">
                                                                    <h2 class="heading"><?php echo $array['fraction'][0]['name']; ?></h2>
                                                                </div><!-- .cbr-headings -->

                                                                <div class="cbr-spacer clearfix" data-desktop="20" data-mobi="15" data-smobi="15"></div>

                                                                <div class="cbr-text">
                                                                    <p class="line-height-30"><?php echo $array['fraction'][0]['description']; ?></p>

                                                                    <div class="cbr-spacer clearfix" data-desktop="28" data-mobi="0" data-smobi="0"></div>

                                                                    <div class="post-meta-option">
                                                                        <div class="row gx-0 align-items-center">
                                                                            <div class="col-md-7 col-12">
                                                                                <div class="post-tag">
                                                                                    <span> <i class="flaticon-supermarket"></i>Tipo de Fracción:</span>
                                                                                    <ul class="tag-list list-style">
                                                                                        <li><a href="#"><?php echo $array['fraction'][0]['type']; ?></a></li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5 col-12 text-md-end text-start">
                                                                                <div class="post-share w-100">
                                                                                    <span>Siguenos en:</span>
                                                                                    <ul class="social-profile style2 list-style">
                                                                                        <li>
                                                                                            <a target="_blank" href="https://www.facebook.com/municipiomixtequilla">
                                                                                                <i class="ri-facebook-fill"></i>
                                                                                            </a>
                                                                                        </li>
                                                                                        <!-- <li>
                                                                                                    <a target="_blank" href="https://twitter.com">
                                                                                                        <i class="ri-twitter-fill"></i>
                                                                                                    </a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a target="_blank" href="https://linkedin.com">
                                                                                                        <i class="ri-linkedin-fill"></i>
                                                                                                    </a>
                                                                                                </li> -->
                                                                                        <li>
                                                                                            <a target="_blank" href="https://www.instagram.com/municipiomixtequilla/">
                                                                                                <i class="ri-instagram-fill"></i>
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- .text-wrap -->

                                                            </div>
                                                        </div><!-- .cbr-content-box -->
                                                    </div><!-- .col-md-8 -->

                                                </div>

                                            </div>

                                    <?php endif;
                                    endif; ?>

                                    <!-- informacion de la fraccion -->
                                    <div class="col-md-12">

                                        <div class="cbr-spacer clearfix" data-desktop="15" data-mobi="0" data-smobi="0"></div>

                                        <div class="cbr-content-box gray-bg">
                                            <div>

                                                <div class="infor-wrap clearfix">
                                                    <div class="project-infor text-center d-flex flex-wrap flex-md-nowrap justify-content-center justify-content-md-between my-4 p-5">

                                                        <div>
                                                            <!-- <i class="fa fa-calendar mb-3" style="font-size: 10rem;"></i> -->
                                                            <i class="ri-calendar-line" style="font-size: 10rem;"></i>
                                                            <h5 class="font-size-14 m-0">Fecha de Actualización</h5>
                                                            <span><?php echo $diaa . ' de ' . $mes[$mesa - 1] . ' del ' . $anioa; ?></span>
                                                        </div>

                                                        <div>
                                                            <!-- <i class="fa fa-calendar-check-o mb-3" style="font-size: 10rem;"></i> -->
                                                            <i class="ri-calendar-check-line" style="font-size: 10rem;"></i>
                                                            <h5 class="font-size-14 m-0">Fecha de Validación</h5>
                                                            <span><?php echo $diav . ' de ' . $mes[$mesv - 1] . ' del ' . $aniov; ?></span>
                                                        </div>

                                                        <div>
                                                            <!-- <i class="fa fa-bell-o mb-3" style="font-size: 10rem;"></i> -->
                                                            <i class="ri-notification-line" style="font-size: 10rem;"></i>
                                                            <h5 class="font-size-14 m-0">Periodo de Actualización</h5>
                                                            <span><?php echo $array['fraction'][0]['period']; ?></span>
                                                        </div>

                                                        <div>
                                                            <!-- <i class="fa fa-users mb-3" style="font-size: 10rem;"></i> -->
                                                            <i class="ri-team-fill" style="font-size: 10rem;"></i>
                                                            <h5 class="font-size-14 m-0">Area responsable de la información</h5>
                                                            <span><?php echo $array['fraction'][0]['departments']; ?></span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="cbr-spacer clearfix" data-desktop="0" data-mobi="35" data-smobi="35"></div>
                                    </div><!-- .col-md-12 -->

                                </div>

                                <div class="col-xs-12">
                                    <div class="cbr-spacer clearfix" data-desktop="110" data-mobi="60" data-smobi="60"></div>
                                </div><!-- .col-md-12 -->

                            </div><!-- .row -->
                        </div> <!-- .container -->
                    </div><!-- .Portfolio-details -->

                </div><!-- inner-content -->
            </div>
            <!--#site-content -->
        </div><!-- #content-wrap -->
    </div><!-- #main-content -->

</div>
<!-- Content Wrapper End -->

<script src="assets/js/transparencia/period.js"></script>