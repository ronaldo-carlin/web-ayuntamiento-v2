<?php
echo '<pre>';
print_r($_POST);
echo '</pre>';

$entity =  new NewsData();
foreach ($_POST as $k => $v) {
    $entity->$k = $v;
    # code...
}

$publish = 0;

if (isset($_POST["publish"])) {
    $publish = 1;
}
if (!isset($_POST["link"])) {
    $entity->link = '';
}
$handle = new Upload($_FILES['video']);
if ($handle->uploaded) {
    $url = "./files/videosNews/";
    $handle->Process($url);
    $entity->video = $handle->file_dst_name;
}
$handle = new Upload($_FILES['cover_page']);
if ($handle->uploaded) {
    $url = "./files/imgNews/";
    $handle->Process($url);
    $entity->cover_page = $handle->file_dst_name;
}

$files = array_filter($_FILES['multimedia']['name']);
$total_count = count($_FILES['multimedia']['name']);
for ($i = 0; $i < $total_count; $i++) {
    $tmpFilePath = $_FILES['multimedia']['tmp_name'][$i];
    if ($tmpFilePath != "") {
        $newFilePath = "./files/imgNews/" . $_FILES['multimedia']['name'][$i];
        if (move_uploaded_file($tmpFilePath, $newFilePath)) {
        }
    }
}
$entity->multimedia = implode(',',  $_FILES['multimedia']['name']);


$entity->created_by = $_SESSION["user_id"];
$entity->description = $_POST["editor2"];
$entity->type = 1;
$entity->publish = $publish;
$entity->companies_avalibles = implode(',', $_POST['companies_avalibles']);
$entity->addAPI();
Core::redir("index.php?view=newsadmin");
