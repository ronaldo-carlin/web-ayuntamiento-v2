<?php
$data = array();
header('Content-type: application/json; charset=utf-8');

if ($_POST['opt'] == 'element') {
     $element = $_POST['id'];

     $data = ElementData::getAllByPeriod($element);
}

if ($_POST['opt'] == 'period') {
     $section = $_POST['id'];

     $data = PeriodData::getAllBySubsection($section);
}

echo json_encode($data);