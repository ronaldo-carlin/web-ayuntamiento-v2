<?php
$cooming = ($_GET['view'] == 'coming_soon') ? 'style="display:none"' : 'style="display:block"'
?>


<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Basic Page Needs -->
    <meta charset="utf-8">

    <!-- Title -->
    <title>Ignacio de la llave | Mixtequilla.</title>

    <!-- Favicon and Touch Icons  -->
    <link rel="shortcut icon" href="assets/img/favicon.png"> <!-- icono web -->
    <link rel="apple-touch-icon-precomposed" href="assets/img/favicon.png"> <!-- icono dispositivo movil -->

    <!-- windows -->
    <meta name="msapplication-TileColor" content="#00A9A4"> <!-- color para windows -->
    <meta name="msapplication-TileImage" content="assets/img/favicon.png"> <!-- icono para windows -->

    <!-- Información al compartir el link -->
    <!-- [if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif] -->
    <meta property="site_name" content="Ignacio de la llave | Mixtequilla"> <!-- Nombre del Sitio -->
    <meta property="title" content="Ignacio de la llave | Mixtequilla" /> <!-- Titulo para whatsapp -->
    <meta name="description" content="Mixtequilla"> <!-- Descripcion para whatsapp -->

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="theme-color" content="#00A9A4"> <!-- Color de Navegador -->

    <meta property="url" content="https://ignaciodelallave.gob.mx" /> <!-- URL -->
    <meta property="locale" content="es_MX" /> <!-- Localidad -->
    <meta property="language" content="ES" /> <!-- Lenguaje -->

    <meta name="keywords" content="agency, agency Portfolio, agency template, creative agency, creative business, creative multipurpose, creative Portfolio, creative html, digital, marketing agency, modern business, multipurpose, Portfolio showcase, professional website,"> <!-- palabras claves -->
    <meta name="author" content="https://github.com/ronaldo-carlin"> <!-- creador de la pagina -->

    <!-- SEO -->
    <link rel="dns-prefetch" href="//www.google.com">
    <link rel="dns-prefetch" href="//s.w.org">

    <!-- bootstrap 5 -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- bootstrap icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">

    <!-- font awesome -->
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

    <?php if (($_GET['view'] == 'coming_soon')) : ?>

        <link rel="stylesheet" type="text/css" href="assets/css/coming_soon.css" media="screen">

    <?php else : ?>

        <!-- Link of CSS files -->

        <!-- Icon Fonts -->
        <link rel="stylesheet" href="assets/css/flaticon.css">

        <link rel="stylesheet" href="assets/css/remixicon.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/css/odometer.min.css">
        <link rel="stylesheet" href="assets/css/fancybox.css">
        <link rel="stylesheet" href="assets/css/aos.css">

        <!-- Theme Style -->
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/dark-theme.css">
        <link rel="stylesheet" href="assets/css/responsive.css">

        <script src="assets/js/jquery.min.js"></script>

    <?php endif; ?>
</head>

<body>
    <!--Preloader starts-->
    <div class="loader js-preloader">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <!--Preloader ends-->

    <!-- Theme Switcher Start -->
    <div class="switch-theme-mode" <?php echo $cooming ?>>
        <label id="switch" class="switch">
            <input type="checkbox" onchange="toggleTheme()" id="slider">
            <span class="slider round"></span>
        </label>
    </div>
    <!-- Theme Switcher End -->

    <div class="page-wrapper">

        <!-- Header Section Start -->
        <header class="header-wrap style1" <?php echo $cooming ?>>

            <div class="header-top">
                <button type="button" class="close-sidebar">
                    <i class="ri-close-fill"></i>
                </button>
                <div class="container">
                    <div class="row align-items-center">

                        <div class="col-lg-8 col-md-12">
                            <div class="header-top-left">
                                <ul class="contact-info list-style">
                                    <li><i class="flaticon-call"></i> <a href="tel:2859760017">(285) 976 0017</a></li>
                                    <li><i class="flaticon-email-1"></i> <a href="mailto:ayuntamiento.mixtequilla@gmail.com">ayuntamiento.mixtequilla@gmail.com</a></li>
                                    <li><i class="flaticon-pin"></i>
                                        <p>Calle César Uscanga 668, Centro, 95240 Ignacio de la Llave, Ver.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-12">
                            <div class="header-top-right">
                                <ul class="header-top-menu list-style">
                                    <li><a href="contact.html">Soporte</a></li>
                                    <li><a href="contact.html">Ayuda</a></li>
                                </ul>
                                <!-- <div class="select-lang">
                                    <i class="ri-global-line"></i>
                                    <div class="navbar-option-item navbar-language dropdown language-option">
                                        <button class="dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="lang-name"></span>
                                        </button>
                                        <div class="dropdown-menu language-dropdown-menu">
                                            <a class="dropdown-item" href="#">
                                                <img src="assets/img/uk.png" alt="flag">
                                                English
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <img src="assets/img/china.png" alt="flag">
                                                简体中文
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <img src="assets/img/uae.png" alt="flag">
                                                العربيّة
                                            </a>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="header-bottom">
                <div class="container">
                    <nav class="navbar navbar-expand-md navbar-light">
                        <a class="navbar-brand" href="./index.php?view=home">
                            <img class="logo-light" src="assets/img/logo-min.png" alt="logo" style="width: 230px;">
                            <img class="logo-dark" src="assets/img/logo blanco.png" alt="logo" style="width: 230px;">
                        </a>
                        <div class="collapse navbar-collapse main-menu-wrap" id="navbarSupportedContent">
                            <div class="menu-close xl-none">
                                <a href="javascript:void(0)"> <i class="ri-close-line"></i></a>
                            </div>
                            <ul class="navbar-nav ms-auto">

                                <li class="nav-item">
                                    <a href="./index.php?view=covid" class="nav-link" id="li-covid">
                                        COVID-19
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="#" class="nav-link" target="_blank" id="li-dif">
                                        DIF Municipal
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="./index.php?view=directorio" class="nav-link" id="li-directorio">
                                        Directorio
                                    </a>
                                </li>

                                <li class="nav-item  has-dropdown">
                                    <a href="#" class="nav-link" id="li-prensa">
                                        Sala de Prensa
                                        <i class="ri-arrow-down-s-line"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="./index.php?view=convocatorias" class="nav-link" id="li-convocatoria">Convocatoria</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="./index.php?view=noticias" class="nav-link" id="li-noticias">Noticias</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="./index.php?view=campanas" class="nav-link" id="li-campanas">Campañas</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="nav-item has-dropdown">
                                    <a href="#" class="nav-link" id="li-tramites">
                                        Tramites y Servicios
                                        <i class="ri-arrow-down-s-line"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="./index.php?view=atencion_ciudadana" class="nav-link" id="li-at_ciudadana">Atención ciudadana</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="./index.php?view=atencion_a_migrantes" class="nav-link" id="li-at_migrantes">Atención a Migrantes</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="./index.php?view=cercano_a_ti" class="nav-link" id="li-cercano">Cercano a ti</a>
                                        </li>
                                        <!-- <li class="nav-item">
                                            <a href="./index.php?view=comercio" class="nav-link" id="li-comercio">Comercio</a>
                                        </li> -->
                                        <li class="nav-item">
                                            <a href="./index.php?view=predial" class="nav-link" id="li-predial">Predial</a>
                                        </li>
                                        <!-- <li class="nav-item">
                                            <a href="./index.php?view=informacion_financiera" class="nav-link">Información Financiera</a>
                                        </li> -->
                                    </ul>
                                </li>

                                <li class="nav-item has-dropdown">
                                    <a href="#" class="nav-link" id="li-transparencia">
                                        Transparencia
                                        <i class="ri-arrow-down-s-line"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="./index.php?view=transparencia_obligaciones_comunes" class="nav-link" id="li-comunes">Ley 875 Art 15 <span>(Obligaciones Comúnes)</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="./index.php?view=transparencia_obligaciones_especificas" class="nav-link" id="li-especificas">Ley 875 Art 16 <span>(Obligaciones Específicas)</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link" id="li-datos">Datos Personales</a>
                                            <ul class="dropdown-menu">
                                                <li class="nav-item">
                                                    <a href="./index.php?view=aviso_de_privacidad" class="nav-link" id="li-privacidad">Aviso de privacidad</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <!-- <li class="nav-item has-dropdown">
                                    <a href="#" class="nav-link">
                                        Blog
                                        <i class="ri-arrow-down-s-line"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                Blog Layout
                                                <i class="ri-arrow-down-s-line"></i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li class="nav-item">
                                                    <a href="blog-no-sidebar.html" class="nav-link">Blog Grid</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="blog-left-sidebar.html" class="nav-link">Blog Left Sidebar</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="blog-right-sidebar.html" class="nav-link">Blog Right Sidebar</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                Single Blog
                                                <i class="ri-arrow-down-s-line"></i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li class="nav-item">
                                                    <a href="blog-details-no-sidebar.html" class="nav-link">Blog Details No Sidebar</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="blog-details-left-sidebar.html" class="nav-link">Blog Details Left Sidebar</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="blog-details-right-sidebar.html" class="nav-link">Blog Details Right Sidebar</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li> -->

                                <!-- <li class="nav-item">
                                    <a href="contact.html" class="nav-link">Contact Us</a>
                                </li>

                                <li class="nav-item xl-none">
                                    <a href="register.html" class="btn style1">Register Now</a>
                                </li> -->

                            </ul>
                            <!-- <div class="others-options  lg-none">
                                <div class="searchbox">
                                    <input type="search" placeholder="Search">
                                    <button type="button">
                                        <i class="flaticon-search"></i>
                                    </button>
                                </div>
                                <div class="header-btn lg-none">
                                    <a href="register.html" class="btn style1">Register Now</a>
                                </div>
                            </div> -->
                        </div>
                    </nav>
                    <div class="mobile-bar-wrap">
                        <div class="mobile-sidebar">
                            <i class="ri-menu-4-line"></i>
                        </div>
                        <button class="searchbtn xl-none" type="button">
                            <i class="flaticon-search"></i>
                        </button>
                        <div class="mobile-menu xl-none">
                            <a href="javascript:void(0)"><i class="ri-menu-line"></i></a>
                        </div>
                    </div>
                </div>
                <div class="search-area">
                    <div class="container">
                        <form action="#">
                            <div class="form-group">
                                <input type="search" placeholder="Search Here" autofocus>
                            </div>
                        </form>
                        <button type="button" class="close-searchbox">
                            <i class="ri-close-line"></i>
                        </button>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header Section End -->

        <?php View::load("index") ?>

        <!-- Footer Section Start -->
        <footer class="footer-wrap bg-rock" <?php echo $cooming ?>>
            <div class="container">
                <img src="assets/img/footer-shape-1.png" alt="Image" class="footer-shape-one">
                <img src="assets/img/footer-shape-2.png" alt="Image" class="footer-shape-two">

                <div class="row justify-content-center pt-100">

                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                        <div class="footer-widget justify-content-center">
                            <a href="index.html" class="footer-logo">
                                <img src="assets/img/logo blanco.png" alt="Image">
                            </a>
                            <!-- <p class="comp-desc">
                                On the other hand, we denounce whteous indig nation and dislike men wh beguiled moraized er hand consec teturus adipis iscing elit eiusmod tempordunt labore dolore magna aliqua consector tetur adip iscing.
                            </p> -->
                            <div class="social-link">
                                <ul class="social-profile list-style style1">
                                    <li>
                                        <a target="_blank" href="https://facebook.com">
                                            <i class="ri-facebook-fill"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://twitter.com">
                                            <i class="ri-twitter-fill"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://linkedin.com">
                                            <i class="ri-linkedin-fill"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://instagram.com">
                                            <i class="ri-pinterest-fill"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                        <div class="footer-widget">
                            <h3 class="footer-widget-title">Menú</h3>
                            <ul class="footer-menu list-style">
                                <li>
                                    <a href="about.html">
                                        <i class="flaticon-next"></i>
                                        Atención Ciudadana
                                    </a>
                                </li>
                                <li>
                                    <a href="service-one.html">
                                        <i class="flaticon-next"></i>
                                        Dif Municipal
                                    </a>
                                </li>
                                <li>
                                    <a href="team.html">
                                        <i class="flaticon-next"></i>
                                        Directorio
                                    </a>
                                </li>
                                <li>
                                    <a href="pricing.html">
                                        <i class="flaticon-next"></i>
                                        Noticias
                                    </a>
                                </li>
                                <!-- <li>
                                    <a href="contact.html">
                                        <i class="flaticon-next"></i>
                                        Contact Us
                                    </a>
                                </li>
                                <li>
                                    <a href="privacy-policy.html">
                                        <i class="flaticon-next"></i>
                                        Privacy Policy
                                    </a>
                                </li> -->
                            </ul>
                        </div>
                    </div>

                    <!-- <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                        <div class="footer-widget">
                            <h3 class="footer-widget-title">Products</h3>
                            <ul class="footer-menu list-style">
                                <li>
                                    <a href="service-one.html">
                                        <i class="flaticon-next"></i>
                                        Online Payment
                                    </a>
                                </li>
                                <li>
                                    <a href="service-one.html">
                                        <i class="flaticon-next"></i>
                                        Deposit Skim
                                    </a>
                                </li>
                                <li>
                                    <a href="service-one.html">
                                        <i class="flaticon-next"></i>
                                        Online Shopping
                                    </a>
                                </li>
                                <li>
                                    <a href="service-one.html">
                                        <i class="flaticon-next"></i>
                                        Master Card
                                    </a>
                                </li>
                                <li>
                                    <a href="service-one.html">
                                        <i class="flaticon-next"></i>
                                        Receive Money
                                    </a>
                                </li>
                                <li>
                                    <a href="service-one.html">
                                        <i class="flaticon-next"></i>
                                        Affiliate Program
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div> -->

                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                        <div class="footer-widget">
                            <h3 class="footer-widget-title">Contacto</h3>
                            <div class="d-flex newsletter-text m-0">
                                <i class="ri-map-2-line"></i>
                                <a class="ms-3" href="https://www.google.com.mx/maps/place/Palacio+Municipal+de+Ignacio+de+la+llave,+Veracruz/@18.7242672,-95.9863523,20z/data=!4m12!1m6!3m5!1s0x85c30a1cdcfc8c81:0x4767eba64f31e0a9!2sComandancia+Municipal+de+Ignacio+de+la+Llave!8m2!3d18.7243566!4d-95.9859862!3m4!1s0x0:0x33bade38da7d4437!8m2!3d18.724325!4d-95.9861183" target="_blank">
                                    <p class="newsletter-text">Calle César Uscanga 668, Centro, 95240 <br> Ignacio de la Llave, Ver.</p>
                                </a>
                            </div>
                            <div class="d-flex newsletter-text m-0">
                                <i class="ri-mail-line"></i>
                                <a class="ms-3" href="mailto:ayuntamiento.mixtequilla@gmail.com">
                                    <p class="newsletter-text">ayuntamiento.mixtequilla@gmail.com</p>
                                </a>
                            </div>
                            <div class="d-flex newsletter-text m-0">
                                <i class="ri-phone-line"></i>
                                <a class="ms-3" href="tel:2859760017">
                                    <p class="newsletter-text">(285) 976 0017</p>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center contact-bg">
                    <div class="col-12 col-sm-4 d-flex justify-content-center">
                        <div class="footer-widget m-4">
                            <h3 class="footer-widget-title">Protección Civil</h3>
                            <div class="d-flex newsletter-text m-0">
                                <i class="ri-phone-line"></i>
                                <a class="ms-3" href="tel:2859760017">
                                    <p class="newsletter-text m-0">(285) 976 0017</p>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 d-flex justify-content-center">
                        <div class="footer-widget m-4">
                            <h3 class="footer-widget-title">Comandancia</h3>
                            <div class="d-flex newsletter-text m-0">
                                <i class="ri-phone-line"></i>
                                <a class="ms-3" href="tel:2859760017">
                                    <p class="newsletter-text m-0">(285) 976 0017</p>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 d-flex justify-content-center">
                        <div class="footer-widget m-4">
                            <h3 class="footer-widget-title">Servicios de Emergencia</h3>
                            <div class="d-flex newsletter-text m-0">
                                <i class="ri-phone-line"></i>
                                <a class="ms-3" href="tel:2859760017">
                                    <p class="newsletter-text m-0">(285) 976 0017</p>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="copyright-text">
                <p> <i class="ri-copyright-line"></i>
                    <script>
                        /* document.write(new Date().getFullYear()); */
                    </script> H. AYUNTAMIENTO IGNACIO DE LA LLAVE. Todos los derechos reservados
                    <!-- <a href="https://hibootstrap.com/" target="_blank">HiBootstrap</a> -->
                </p>
            </div>
        </footer>
        <!-- Footer Section End -->

    </div>

    <!-- Back-to-top button Start -->
    <a href="javascript:void(0)" class="back-to-top bounce"><i class="ri-arrow-up-s-line"></i></a>
    <!-- Back-to-top button End -->

    <?php if (($_GET['view'] == 'coming_soon')) : ?>

        <script src="assets/js/coming_soon.js"></script>

    <?php else : ?>
        
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/form-validator.min.js"></script>
        <script src="assets/js/contact-form-script.js"></script>
        <script src="assets/js/aos.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/odometer.min.js"></script>
        <script src="assets/js/fancybox.js"></script>
        <script src="assets/js/jquery.appear.js"></script>
        <script src="assets/js/tweenmax.min.js"></script>
        <script src="assets/js/main.js"></script>
    <?php endif; ?>

    <!-- Link of JS files -->



</body>

</html>