<?php
class ElementData
{
    public static $tablename = "Element";



    public function __construct()
    {
    }

    // * Trae los elementos del periodo
    public static function getAllByPeriod($subsection)
    {

        $url = $GLOBALS["api"] . '/getAllByPeriod?section_id=' . $subsection;
        //echo $url;
        $json = file_get_contents($url);
        return json_decode($json, true);
    }
}
