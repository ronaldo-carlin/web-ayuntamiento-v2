<?php
class PeriodData
{
	public static $tablename = "period";



	public function __construct()
	{
	}

	//* Crea el usuario
	public  function addAPI()
	{

		$url = $GLOBALS["api"] . '/createUser';

		$data = [
			'name' => $this->name,
			'email' => $this->email,
			'password' => $this->password,
			'phone' => $this->phone,
			'enrollment' => $this->enrollment,
			'curp' => $this->curp,
			'rfc' => $this->rfc,
			'imss' => $this->imss,
			'area' => $this->area,
			'department' => $this->department,
			'role' => $this->role,
			'address' => $this->address,
			'date_birth' =>   $this->date_birth,
			'date_admission' =>  $this->date_admission,
			'company_id' => $this->company_id,
			'role_id' => $this->role_id,
			'img' => $this->img,
			'created_by' =>   $this->created_by,

		];
		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}
	//* Trae los usuarios activos
	public static function loginAPI($email, $password)
	{

		$url = $GLOBALS["api"] . '/login?email=' . $email . '&password=' . $password;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	//* Trae los usuarios activos
	public static function getUsersActivesAPI()
	{

		$url = $GLOBALS["api"] . '/getUsersActives';
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	//* Trae el usuario por id
	public static function getUserByIdAPI($id)
	{

		$url = $GLOBALS["api"] . '/getUserById?id=' . $id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	//* Trae los usuarios por compañia
	public static function getUsersByCompanyAPI($company_id)
	{

		$url = $GLOBALS["api"] . '/getUsersByCompany?company_id=' . $company_id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	//* Trae los ultimos 3 cumpleaños y los proximos 3 cumpleaños por compañia
	public static function getBirthdaysAPI($company_id)
	{

		$url = $GLOBALS["api"] . '/getBirthdays?company_id=' . $company_id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	//* Trae los los recibos
	public static function getInvoice($enrollment)
	{

		$url = $GLOBALS["api"] . '/getInvoice?enrollment=' . $enrollment;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	//* Trae el pdf
	public static function getArchive($id)
	{

		$url = $GLOBALS["api"] . '/getArchive?id=' . $id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	//* Trae el pdf
	public static function getAllBySubsection($id)
	{

		$url = $GLOBALS["api"] . '/getAllBySubsection?section=' . $id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

}
