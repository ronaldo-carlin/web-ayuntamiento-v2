<?php
class UserData
{
	public static $tablename = "users";



	public function __construct()
	{
	}
	public function add()
	{
		$sql = "insert into " . self::$tablename . " (name,email,password,phone,enrollment,curp,rfc,imss,department,role,address,date_birth,date_admission,company_id,created_at,created_by,role_id,area,img)";
		$sql .= "value (\"$this->name\",\"$this->email\",\"$this->password\",\"$this->phone\",\"$this->enrollment\",\"$this->curp\",\"$this->rfc\",\"$this->imss\",\"$this->department\",\"$this->role\",\"$this->address\",\"$this->date_birth\",\"$this->date_admission\",\"$this->company_id\",NOW(),$this->created_by,$this->role_id,\"$this->area\",\"$this->img\")";
		//echo $sql;
		return Executor::doit($sql);
	}

	public static function delById($id)
	{
		$sql = "update " . self::$tablename . " set deleted=1, deleted_at=NOW() where id=$id";
		Executor::doit($sql);
	}
	public function del()
	{
		$sql = "delete from " . self::$tablename . " where id=$this->id";
		//$sql = "update ".self::$tablename." set deleted=1, deleted_at=NOW(), deleted_by=$this->user_id where id=$this->id";
		Executor::doit($sql);
	}

	// partiendo de que ya tenemos creado un objecto UserData previamente utilizamos el contexto
	public function update()
	{
		$sql = "update " . self::$tablename . " set name=\"$this->name\", email=\"$this->email\",phone=\"$this->phone\",enrollment=\"$this->enrollment\",curp=\"$this->curp\",rfc=\"$this->rfc\",imss=\"$this->imss\",area=\"$this->area\",department=\"$this->department\",role=\"$this->role\", address=\"$this->address\",date_birth=\"$this->date_birth\",date_admission=\"$this->date_admission\",company_id=\"$this->company_id\",role_id=\"$this->role_id\",modified_at=NOW(),modified_by=\"$this->modified_by\" where id=$this->id";
		//echo $sql;
		return Executor::doit($sql);
	}
	public static function updateSalary($id, $salary_day, $salary_month, $bonus_day, $days)
	{
		$sql = "update " . self::$tablename . " set salary_day=$salary_day,salary_month=$salary_month, bonus_day=$bonus_day, days=$days  where id=$id";
		Executor::doit($sql);
	}

	public function updateProfile()
	{
		$sql = "update " . self::$tablename . " set name=\"$this->name\",lastname=\"$this->lastname\",phone=\"$this->phone\",email=\"$this->email\",payroll_number=\"$this->payroll_number\",rfc=\"$this->rfc\", is_manager=\"$this->is_manager\", modified_at=NOW(), modified_by=\"$this->user_id\" where id=$this->id";
		Executor::doit($sql);
	}

	public static function update_passwd($id, $password)
	{
		$sql = "update " . self::$tablename . " set password=\"$password\" where id=$id";
		//echo $sql;
		Executor::doit($sql);
	}
	public static function getById($id)
	{
		$sql = "select * from " . self::$tablename . " where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0], new UserData());
	}
	public static function getByMail($mail)
	{
		$sql = "select * from " . self::$tablename . " where email=\"$mail\"";
		$query = Executor::doit($sql);
		return Model::one($query[0], new UserData());
	}
	public static function getAllByUserId($id)
	{
		$sql = "select * from " . self::$tablename . " where created_by = $id order by name asc";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UserData());
	}
	public static function getAll()
	{
		$sql = "select * from " . self::$tablename . " where deleted=0";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UserData());
	}
	public static function getCount()
	{
		$sql = "select COUNT(ID) as count from " . self::$tablename;
		$query = Executor::doit($sql);
		return Model::one($query[0], new UserData());
	}
	public static function getByPositionId($id)
	{
		$sql = "select * from " . self::$tablename . " where position_id = $id";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UserData());
	}
	public  static function getByDateBirth()
	{
		$sql = "select * from " . self::$tablename . "  where  day(date_birth)=day(CURDATE()) and MONTH(date_birth)=MONTH(CURDATE())";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UserData());
	}

	//* Crea el usuario
	public  function addAPI()
	{

		$url = $GLOBALS["api"] . '/createUser';

		$data = [
			'name' => $this->name,
			'email' => $this->email,
			'password' => $this->password,
			'phone' => $this->phone,
			'enrollment' => $this->enrollment,
			'curp' => $this->curp,
			'rfc' => $this->rfc,
			'imss' => $this->imss,
			'area' => $this->area,
			'department' => $this->department,
			'role' => $this->role,
			'address' => $this->address,
			'date_birth' =>   $this->date_birth,
			'date_admission' =>  $this->date_admission,
			'company_id' => $this->company_id,
			'role_id' => $this->role_id,
			'img' => $this->img,
			'created_by' =>   $this->created_by,

		];
		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}
	//* Trae los usuarios activos
	public static function loginAPI($email, $password)
	{

		$url = $GLOBALS["api"] . '/login?email=' . $email . '&password=' . $password;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	//* Trae los usuarios activos
	public static function getUsersActivesAPI()
	{

		$url = $GLOBALS["api"] . '/getUsersActives';
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	//* Trae el usuario por id
	public static function getUserByIdAPI($id)
	{

		$url = $GLOBALS["api"] . '/getUserById?id=' . $id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	//* Trae los usuarios por compañia
	public static function getUsersByCompanyAPI($company_id)
	{

		$url = $GLOBALS["api"] . '/getUsersByCompany?company_id=' . $company_id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	//* Trae los ultimos 3 cumpleaños y los proximos 3 cumpleaños por compañia
	public static function getBirthdaysAPI($company_id)
	{

		$url = $GLOBALS["api"] . '/getBirthdays?company_id=' . $company_id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	//* Trae los los recibos
	public static function getInvoice($enrollment)
	{

		$url = $GLOBALS["api"] . '/getInvoice?enrollment=' . $enrollment;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	//* Trae el pdf
	public static function getArchive($id)
	{

		$url = $GLOBALS["api"] . '/getArchive?id=' . $id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	public static function getEvalByCompanyIdAndCodeId($company_id, $code_id)
	{
		$sql = "select * from " . self::$tablename . " where company_id =$company_id and position_id = $code_id";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UserData());
	}
	public static function showCova($id, $show)
	{
		$sql = "update " . self::$tablename . " set showConva=$show where id=$id";
		Executor::doit($sql);
	}

	public static function getLike($q)
	{
		$sql = "select * from " . self::$tablename . " where name like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UserData());
	}

	public static function getPermissonsByUserAndModule($id, $module_id)
	{
		$sql = "select u.role_id AS 'role', s.created AS 'create' ,s.read AS 'read', s.edit AS 'edit', s.delete AS 'delete' from " . self::$tablename . ' u' . "  INNER JOIN  role_settings s ON s.role_id = u.role_id  where u.id =$id and s.module_id=$module_id";
		//echo $sql;
		$query = Executor::doit($sql);
		return Model::one($query[0], new RolessettingsData());
	}

	//* Funciones viejas
	/*public  static function getByMonthBirth($id)
	{
		$sql = "select * from " . self::$tablename . "   where company_id=$id  and  MONTH(date_birth)=MONTH(NOW())  and day(date_birth)<= day('" . date('Y-m-d', strtotime('+3 days')) . "')   AND day(date_birth) >= day('" . date('Y-m-d', strtotime('-3 days')) . "') order by day(date_birth) asc";
		echo $sql;
		$query = Executor::doit($sql);
		return Model::many($query[0], new UserData());
	}*/
	/* public static function getByCompany($id)
	{
		$sql = "select * from " . self::$tablename . " where company_id = $id";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UserData());
	} */
}
