<?php

class NewsData
{
	public static $tablename = "news";

	

	public function __construct()
	{
	}
	public function addWeb()
	{
		$sql = "insert into " . self::$tablename . " (name, description, cover_page, publish, company_id ,created_at,created_by,multimedia,type,link,video) ";
		$sql .= "value (\"$this->name\", \"$this->description\", \"$this->cover_page\",\"$this->publish\",\"$this->company_id\",  NOW(),$this->user_id,\"$this->multimedia\",\"$this->type\",\"$this->link\",\"$this->video\")";
		//echo $sql;
		Executor::doit($sql);
	}
	public static function delById($id)
	{
		$sql = "delete from " . self::$tablename . " where id=$id";
		Executor::doit($sql);
	}
	public static function  del($id)
	{
		//$sql = "delete from ".self::$tablename." where id=$this->id";
		$sql = "update " . self::$tablename . " set deleted=1, deleted_at=NOW() where id=$id";
		echo $sql;
		return Executor::doit($sql);
	}
	public function update()
	{
		$sql = "update " . self::$tablename . " set name=\"$this->name\",company_id=\"$this->company_id\",description=\"$this->description\", date_validity=\"$this->date_validity\",publish=\"$this->publish\",companies_avalibles=\"$this->companies_avalibles\", update_at=NOW(), update_by=$this->update_by where id=$this->id";
		echo $sql;
		//return Executor::doit($sql);
	}
	public function updateNewsWeb()
	{
		$sql = "update " . self::$tablename . " set name=\"$this->name\",company_id=\"$this->company_id\",description=\"$this->description\",publish=\"$this->publish\", update_at=NOW(), update_by=$this->update_by, link=\"$this->link\" where id=$this->id";
		//echo $sql;
		return Executor::doit($sql);
	}
	public static function updateCoverPage($id, $cover_page)
	{
		$sql = "update " . self::$tablename . " set cover_page = \"$cover_page\" where id=$id";
		//echo $sql;
		return Executor::doit($sql);
	}
	public static function updateMultimedia($id, $multimedia)
	{
		$sql = "update " . self::$tablename . " set multimedia=\"$multimedia\" where id=$id";
		echo $sql;
		//return Executor::doit($sql);
	}
	public function updateLink($id, $link)
	{
		$sql = "update " . self::$tablename . " set link=$link where id=$id";
		echo $sql;
		return Executor::doit($sql);
	}
	public static function updateVideo($id, $video)
	{
		$sql = "update " . self::$tablename . " set video=\"$video\" where id=$id";
		//echo $sql;
		return Executor::doit($sql);
	}
	public static function publish($id)
	{
		$sql = "update " . self::$tablename . " set publish=1 where id=$id";
		//echo $sql;
		$query = Executor::doit($sql);
	}
	public static function unpublish($id)
	{
		$sql = "update " . self::$tablename . " set publish=0 where id=$id";
		//echo $sql;
		$query = Executor::doit($sql);
	}

	//* Crea la noticia
	public  function addAPI()
	{

		$url = $GLOBALS["api"] . '/createNew';
		if ($this->type == 1) {
			$data = [
				'name' => $this->name,
				'description' => $this->description,
				'cover_page' => $this->cover_page,
				'link' => $this->link,
				'type' => $this->type,
				'multimedia' => $this->multimedia,
				'video' => $this->video,
				'date_validity' =>  $this->date_validity,
				'created_by' =>   $this->created_by,
				'company_id' => $this->company_id,
				'publish' => $this->publish,
				'companies_avalibles' => $this->companies_avalibles,

			];
		} else {
			$data = [
				'name' => $this->name,
				'description' => $this->description,
				'cover_page' => $this->cover_page,
				'link' => $this->link,
				'type' => $this->type,
				'multimedia' => $this->multimedia,
				'video' => $this->video,
				'created_by' =>   $this->created_by,
				'company_id' => $this->company_id,
				'publish' => $this->publish,

			];
		}

		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}
	//* Actualiza la noticia
	public  function updateAPI()
	{

		$url = $GLOBALS["api"] . '/updateNew?id=' . $this->id;

		if ($this->type == 1) {
			$data = [
				'name' => $this->name,
				'description' => $this->description,
				'cover_page' => $this->cover_page,
				'link' => $this->link,
				'type' => $this->type,
				'multimedia' => $this->multimedia,
				'video' => $this->video,
				'date_validity' =>  $this->date_validity,
				'update_by' =>   $this->update_by,
				'company_id' => $this->company_id,
				'publish' => $this->publish,
				'companies_avalibles' => $this->companies_avalibles,

			];
		} else {
			$data = [
				'name' => $this->name,
				'description' => $this->description,
				'cover_page' => $this->cover_page,
				'link' => $this->link,
				'type' => $this->type,
				'multimedia' => $this->multimedia,
				'video' => $this->video,
				'update_by' =>   $this->update_by,
				'company_id' => $this->company_id,
				'publish' => $this->publish,

			];
		}


		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}
	//* Borra la noticia
	public static function deletedAPI($id)
	{
		$url = $GLOBALS["api"] . '/deleteNew?id=' . $id;

		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}
	//* Publica la noticia
	public static function publishAPI($id)
	{
		$url = $GLOBALS["api"] . '/publishNew?id=' . $id;

		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}
	//* Despublica la noticia
	public static function unpublishAPI($id)
	{
		$url = $GLOBALS["api"] . '/unpublishNew?id=' . $id;

		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}
	// * Trae todas las noticias activas
	public static function getActivesAPI()
	{
		$url = $GLOBALS["api"] . '/getNewsActives';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	// * Trae las noticias wen
	public static function getWebNewsAPI()
	{

		$url = $GLOBALS["api"] . '/getWebNews';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	// * Trae la noticia por id
	public static function getByIdAPI($id)
	{

		$url = $GLOBALS["api"] . '/getNewsById?id=' . $id;
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	public static function getAllNewsWebPublish()
	{
		$sql = "select * from " . self::$tablename . " where type=2    and deleted = 0";
		$query = Executor::doit($sql);
		return Model::many($query[0], new NewsData());
	}

	//* Trae las noticias por compañia
	public static function getByCompanyAPI($company_id)
	{


		$url = $GLOBALS["api"] . '/getNewsByCompany?company_id=' . $company_id;
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	public static function getPublish()
	{
		$sql = "select * from " . self::$tablename . " where publish=1";
		$query = Executor::doit($sql);
		return Model::one($query[0], new NewsData());
	}
	public static function getAll()
	{
		$sql = "select * from " . self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0], new NewsData());
	}

	public static function getActives()
	{
		$sql = "select * from " . self::$tablename . " where deleted = 0 and type=1";
		$query = Executor::doit($sql);
		return Model::many($query[0], new NewsData());
	}
	public static function getAllCount()
	{
		$sql = "select count(id) as count from " . self::$tablename . " where  type=1";
		$query = Executor::doit($sql);
		return Model::one($query[0], new PromotionsData());
	}
	public static function getPublishCount()
	{
		$sql = "select count(id) as count from " . self::$tablename . " where publish =1 and type=1 ";
		$query = Executor::doit($sql);
		return Model::one($query[0], new PromotionsData());
	}
	public static function getNopublishCount()
	{
		$sql = "select count(id) as count from " . self::$tablename . " where publish =0 and type=1 ";
		$query = Executor::doit($sql);
		return Model::one($query[0], new PromotionsData());
	}

	//* Funciones viejas

	/* public static function getByCNCI()
	{
		$sql = "select * from " . self::$tablename . " where company_id=1 and publish=1 and type=1";
		//echo $sql;
		$query = Executor::doit($sql);
		return Model::many($query[0], new NewsData());
	} */
	/* public static function getByCoporativo()
	{
		$sql = "select * from " . self::$tablename . " where company_id=2 and publish=1 and type=1";
		$query = Executor::doit($sql);
		return Model::many($query[0], new NewsData());
	} */
	/* public static function getBySuperBoletos()
	{
		$sql = "select * from " . self::$tablename . " where company_id=3 and publish=1 and type=1";
		$query = Executor::doit($sql);
		return Model::many($query[0], new NewsData());
	} */
	/* public static function getByArenaMty()
	{
		$sql = "select * from " . self::$tablename . " where company_id=4 and publish=1 and type=1";
		$query = Executor::doit($sql);
		return Model::many($query[0], new NewsData());
	} */
	/* public static function getByArenaMx()
	{
		$sql = "select * from " . self::$tablename . " where company_id=5 and publish=1 and type=1";
		$query = Executor::doit($sql);
		return Model::many($query[0], new NewsData());
	} */
	/* public static function getByArenaGdl()
	{
		$sql = "select * from " . self::$tablename . " where company_id=6 and publish=1 and type=1";
		$query = Executor::doit($sql);
		return Model::many($query[0], new NewsData());
	} */
	/* public static function getByDyce()
	{
		$sql = "select * from " . self::$tablename . " where company_id=7 and publish=1 and type=1";
		$query = Executor::doit($sql);
		return Model::many($query[0], new NewsData());
	} */
	/* public static function getByTodito()
	{
		$sql = "select * from " . self::$tablename . " where company_id=8 and publish=1 and type=1";
		$query = Executor::doit($sql);
		return Model::many($query[0], new NewsData());
	} */
	/* public static function getByAzteca()
	{
		$sql = "select * from " . self::$tablename . " where company_id=9 and publish=1 and type=1";
		$query = Executor::doit($sql);
		return Model::many($query[0], new NewsData());
	} */
	/* public static function getByHorizonte()
	{
		$sql = "select * from " . self::$tablename . " where company_id=10 and publish=1 and type=1";
		$query = Executor::doit($sql);
		return Model::many($query[0], new NewsData());
	} */
	/* public static function getById($id)
	{
		$sql = "select * from " . self::$tablename . " where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0], new NewsData());
	} */
	/* public function add()
	{
		$sql = "insert into " . self::$tablename . " (name, description, date_validity, cover_page, publish, company_id ,created_at,created_by,companies_avalibles,multimedia,type) ";
		$sql .= "value (\"$this->name\", \"$this->description\", \"$this->date_validity\",\"$this->cover_page\",\"$this->publish\",\"$this->company_id\",  NOW(),$this->user_id,\"$this->companies_avalibles\",\"$this->multimedia\",\"$this->type\")";
		//echo $sql;
		Executor::doit($sql);
	} */
}
