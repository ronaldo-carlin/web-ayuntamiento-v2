<?php
$url = 'http://apis.ignaciodelallave.com/getAllNews';
$json = file_get_contents($url);
$array = json_decode($json, true);

/* echo '<pre>';
    //print_r($array['fractions']);
    var_dump($_GET);
    echo '</pre>'; */

$numpag = intval($_GET['page']);
$max = intval($_GET['max']);
$start = ($numpag - 1) * $max;
?>

<?php
$array_news = $array['news'];
for ($i = $start; $i < $start + $max; $i++) :
    if ($i >= $array['count']) {
        break;
    }

    /* FECHA */
    $mes = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    $mesNoticia = date('n', strtotime($array_news[$i]['created_at']));
    $diaNoticia = date('d', strtotime($array_news[$i]['created_at']));
    $anioNoticia = date('Y', strtotime($array_news[$i]['created_at']));
    setlocale(LC_TIME, "spanish");

?>
    <article class="hentry">
        <div class="post-media clearfix">
            <a href="index.php?view=noticia_detallado&id_noticia=<?php echo $array_news[$i]['id'] ?>"><img loading="lazy" src="http://comunidad.ignaciodelallave.com/files/imgNews/<?php echo $array_news[$i]['cover_page']; ?>" alt="Image"></a>
            <!-- <a href="#"><img src="files/news/<?php echo $array_news[$i]['filename']; ?>" alt="Image"></a> -->
        </div>

        <div class="post-content-wrap">
            <h2 class="post-title">
                <span>
                    <a href="index.php?view=noticia_detallado&id_noticia=<?php echo $array_news[$i]['id'] ?>"><?php echo $array_news[$i]['title']; ?></a>
                </span>
            </h2>

            <div class="post-content post-excerpt">
                <a href="index.php?view=noticia_detallado&id_noticia=<?php echo $array_news[$i]['id'] ?>">
                    <p><?php echo substr($array_news[$i]['description'], 0, 400) . '...'; ?></p>
                </a>
            </div>

            <div class="post-meta">
                <div class="post-meta-content">
                    <div class="post-meta-content-inner">
                        <span class="post-by-author item">
                            <span class="inner"><a href="#" title="" rel="author"><?php echo $array_news[$i]['created_by']; ?></a></span>
                        </span>

                        <span class="post-date item">
                            <span class="inner"><span class="entry-date"><?php echo $diaNoticia . ' - ' . $mesNoticia . ' - ' . $anioNoticia; ?></span></span>
                        </span>

                        <!-- <span class="post-comment item">
                            <span class="inner">(02)</span>
                        </span> -->
                    </div>
                </div>
            </div>
        </div>

    </article>

<?php
endfor;
?>