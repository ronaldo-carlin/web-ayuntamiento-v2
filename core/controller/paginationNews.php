<?php
$api = 'https://apis.comunidadmixtequilla.com';
$multimedia = 'https://comunidadmixtequilla.com';

$url = $GLOBALS["api"] . '/getNewsPublic';
$json = file_get_contents($url);
$array = json_decode($json, true);

$numpag = intval($_GET['page']);
$max = intval($_GET['max']);
$start = ($numpag - 1) * $max;
?>

<?php
$array_news = $array['news'];
if ($_GET['type'] == 1) {
    for ($i = $start; $i < $start + $max; $i++) :
        if ($i >= $array['count']) {
            break;
        }

        /* FECHA */
        $mes = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $mesNoticia = date('n', strtotime($array_news[$i]['created_at']));
        $diaNoticia = date('d', strtotime($array_news[$i]['created_at']));
        $anioNoticia = date('Y', strtotime($array_news[$i]['created_at']));
        setlocale(LC_TIME, "spanish");

?>

        <div class="col-lg-6 col-md-6">
            <article>
                <div class="blog-card style1">
                    <div class="blog-img">
                        <a href="index.php?view=noticia_detallada&id_noticia=<?php echo $array_news[$i]['id'] ?>">
                            <!-- <img loading="lazy" src="https://ignaciodelallave.com/files/imgNews/<?php echo $array_news[$i]['cover_page']; ?>" alt="Image"> -->
                            <img loading="lazy" src="<?php echo $GLOBALS['multimedia'] . "/files/imgNews/" . $array_news[$i]['cover_page']; ?>" alt="Image">
                        </a>
                    </div>
                    <div class="blog-info">

                        <ul class="blog-metainfo  list-style">
                            <li><i class="flaticon-user"></i> <a href="index.php?view=noticia_detallada&id_noticia=<?php echo $array_news[$i]['id'] ?>"><?php echo $array_news[$i]['name_user']; ?></a></li>
                            <li><i class="flaticon-calendar"></i><?php echo $diaNoticia . ' - ' . $mesNoticia . ' - ' . $anioNoticia; ?></li>
                        </ul>
                        <h3><a href="index.php?view=noticia_detallada&id_noticia=<?php echo $array_news[$i]['id'] ?>"><?php echo $array_news[$i]['title']; ?></a></h3>
                        <a href="index.php?view=noticia_detallada&id_noticia=<?php echo $array_news[$i]['id'] ?>" class="link style1">Ver Más
                            <i class="flaticon-right-arrow"></i>
                        </a>
                    </div>
                </div>
            </article>
        </div>

    <?php
    endfor;
} elseif ($_GET['type'] == 2) {
    for ($i = $start; $i < $start + $max; $i++) :
        if ($i >= $array['count']) {
            break;
        }

        /* FECHA */
        $mes = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $mesNoticia = date('n', strtotime($array_news[$i]['created_at']));
        $diaNoticia = date('d', strtotime($array_news[$i]['created_at']));
        $anioNoticia = date('Y', strtotime($array_news[$i]['created_at']));
        setlocale(LC_TIME, "spanish");
    ?>
        <div class="col-lg-4 col-md-4">
            <article>
                <div class="blog-card style1">
                    <div class="blog-img">
                        <a href="index.php?view=noticia_detallada&id_noticia=<?php echo $array_news[$i]['id'] ?>">
                            <!-- <img loading="lazy" src="https://ignaciodelallave.com/files/imgNews/<?php echo $array_news[$i]['cover_page']; ?>" alt="Image"> -->
                            <img loading="lazy" src="<?php echo $GLOBALS['multimedia'] . "/files/imgNews/" . $array_news[$i]['cover_page']; ?>" alt="Image">
                        </a>
                    </div>
                    <div class="blog-info">

                        <ul class="blog-metainfo  list-style">
                            <li><i class="flaticon-user"></i> <a href="index.php?view=noticia_detallada&id_noticia=<?php echo $array_news[$i]['id'] ?>"><?php echo $array_news[$i]['name_user']; ?></a></li>
                            <li><i class="flaticon-calendar"></i><?php echo $diaNoticia . ' - ' . $mesNoticia . ' - ' . $anioNoticia; ?></li>
                        </ul>
                        <h3><a href="index.php?view=noticia_detallada&id_noticia=<?php echo $array_news[$i]['id'] ?>"><?php echo $array_news[$i]['title']; ?></a></h3>
                        <a href="index.php?view=noticia_detallada&id_noticia=<?php echo $array_news[$i]['id'] ?>" class="link style1">Ver Más
                            <i class="flaticon-right-arrow"></i>
                        </a>
                    </div>
                </div>
            </article>
        </div>

<?php
    endfor;
} else {
    echo '<pre>';
    print_r('no se encontraron fracciones');
    echo '</pre>';
}
?>