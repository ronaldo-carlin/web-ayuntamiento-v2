<?php
$api = 'https://apis.comunidadmixtequilla.com';
$multimedia = 'https://comunidadmixtequilla.com';

if ($_GET['type'] == 1) {
    $url = $GLOBALS["api"].'/getAllByAdminandtype?admin=1&type=1';
    $json = file_get_contents($url);
    $array = json_decode($json, true);

} elseif ($_GET['type'] == 2) {
    $url = $GLOBALS["api"].'/getAllByAdminandtype?admin=1&type=2';
    $json = file_get_contents($url);
    $array = json_decode($json, true);
} else {
    echo '<pre>';
    print_r('no se encontraron fracciones');
    echo '</pre>';
}

$numpag = intval($_GET['page']);
$max = intval($_GET['max']);
$start = ($numpag - 1) * $max;
?>

<?php
$array_news = $array['fractions'];
for ($i = $start; $i < $start + $max; $i++) :
    if ($i >= $array['count']) {
        break;
    }

    /* FECHA */
    $mes = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    $mesNoticia = date('n', strtotime($array_news[$i]['created_at']));
    $diaNoticia = date('d', strtotime($array_news[$i]['created_at']));
    $anioNoticia = date('Y', strtotime($array_news[$i]['created_at']));
    setlocale(LC_TIME, "spanish");

?>
    <div class="col-xl-4 col-lg-6 col-md-6">
        <div class="service-card text-center style1">
            <a href="./index.php?view=transparencia_detallado&id_fraction=<?php echo $array_news[$i]['id'] ?>">
                <span class="service-icon">
                    <img loading="lazy" src="<?php echo $GLOBALS['multimedia']."/files/iconFraction/".$array_news[$i]['filename']; ?>" alt="Image">
                </span>
                <h3><a href="./index.php?view=transparencia_detallado&id_fraction=<?php echo $array_news[$i]['id'] ?>"><?php echo $array_news[$i]['name']; ?></a></h3>
                <p>Fracción <?php echo $array_news[$i]['fraction']; ?></p>
            </a>
        </div>
    </div>

<?php
endfor;
?>